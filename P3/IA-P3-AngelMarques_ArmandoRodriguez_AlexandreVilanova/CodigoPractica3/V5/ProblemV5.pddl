(define (problem ProblemV5) 
    (:domain RicoRico)

    (:objects p11 p12 p21 p22 p31 p32 p41 p42 p51 p52 p61 p62 p71 p72 p81 p82 p91 p92 - plato
              d1 d2 d3 d4 d5 - dia
              s1 s2 s3 s4 s5 s6 s7 s8 s9 s10 - slot
              o1 o2 - orden
              tPar tImpar - tipo
    )

    (:init
        (= (total-cost) 0)
        
        (ordenPlato p11 o1)
        (ordenPlato p12 o2)
        (ordenPlato p21 o1)
        (ordenPlato p22 o2)
        (ordenPlato p31 o1)
        (ordenPlato p32 o2)
        (ordenPlato p41 o1)
        (ordenPlato p42 o2)
        (ordenPlato p51 o1)
        (ordenPlato p52 o2)
        (ordenPlato p61 o1)
        (ordenPlato p62 o2)
        (ordenPlato p71 o1)
        (ordenPlato p72 o2)
        (ordenPlato p81 o1)
        (ordenPlato p82 o2)
        (ordenPlato p91 o1)
        (ordenPlato p92 o2)
        
        (ordenSlot s1 o1)
        (ordenSlot s2 o2)
        (diaSlot s1 d1)
        (diaSlot s2 d1)
        
        (ordenSlot s3 o1)
        (ordenSlot s4 o2)
        (diaSlot s3 d2)
        (diaSlot s4 d2)
        
        (ordenSlot s5 o1)
        (ordenSlot s6 o2)
        (diaSlot s5 d3)
        (diaSlot s6 d3)
        
        (ordenSlot s7 o1)
        (ordenSlot s8 o2)
        (diaSlot s7 d4)
        (diaSlot s8 d4)
        
        (ordenSlot s9 o1)
        (ordenSlot s10 o2)
        (diaSlot s9 d5)
        (diaSlot s10 d5)
        
        (incompatible p11 p12)
        (incompatible p21 p22)
        (incompatible p31 p32)
        (incompatible p41 p42)
        (incompatible p51 p52)
        (incompatible p61 p62)
        (incompatible p71 p72)
        (incompatible p81 p82)
        (incompatible p91 p92)

        (diaContiguo d1 d2)
        (diaContiguo d2 d1)
        (diaContiguo d2 d3)
        (diaContiguo d3 d2)
        (diaContiguo d3 d4)
        (diaContiguo d4 d3)
        (diaContiguo d4 d5)
        (diaContiguo d5 d4)

        (tipoPlato p11 tImpar)
        (tipoPlato p12 tImpar)
        (tipoPlato p21 tPar)
        (tipoPlato p22 tPar)
        (tipoPlato p31 tImpar)
        (tipoPlato p32 tImpar)
        (tipoPlato p41 tPar)
        (tipoPlato p52 tPar)
        (tipoPlato p51 tImpar)
        (tipoPlato p52 tImpar)
        (tipoPlato p61 tPar)
        (tipoPlato p62 tPar)
        (tipoPlato p71 tImpar)
        (tipoPlato p72 tImpar)
        (tipoPlato p81 tPar)
        (tipoPlato p82 tPar)
        (tipoPlato p91 tImpar)
        (tipoPlato p92 tImpar)
        
        (platoObligado p11)
        (platoObligado p21)
        (platoObligado p31)
        (platoObligado p41)
        (platoObligado p51)

        (platoObligadoDia p11 d1)
        (platoObligadoDia p21 d2)
        (platoObligadoDia p31 d3)
        (platoObligadoDia p41 d4)
        (platoObligadoDia p51 d5)

        (= (caloriasPlato p11) 1400) ; plato obligatorio, tendria que ir con p22
        (= (caloriasPlato p12) 600)
        (= (caloriasPlato p21) 50)   ; plato obligatorio, tendria que ir con p32
        (= (caloriasPlato p22) 100)
        (= (caloriasPlato p31) 600)  ; plato obligatorio
        (= (caloriasPlato p32) 1450)
        (= (caloriasPlato p41) 600)  ; plato obligatorio
        (= (caloriasPlato p42) 600)
        (= (caloriasPlato p51) 600)  ; plato obligatorio
        (= (caloriasPlato p52) 600)
        (= (caloriasPlato p61) 2000) ; este plato no saldra por la extensión 3
        (= (caloriasPlato p62) 600)
        (= (caloriasPlato p71) 2000) ; este plato no saldra por la extensión 3
        (= (caloriasPlato p72) 600)
        (= (caloriasPlato p81) 2000) ; este plato no saldra por la extensión 3
        (= (caloriasPlato p82) 2000) ; este plato no tendria que salir
        (= (caloriasPlato p91) 2000) ; este plato no saldra por la extensión 3
        (= (caloriasPlato p92) 2000) ; este plato no tendria que salir

        (= (precioPlato p11) 20)
        (= (precioPlato p12) 20)
        (= (precioPlato p21) 20)
        (= (precioPlato p22) 20)
        (= (precioPlato p31) 20)
        (= (precioPlato p32) 20)
        (= (precioPlato p41) 20)
        (= (precioPlato p42) 20)
        (= (precioPlato p51) 20)
        (= (precioPlato p52) 20)
        (= (precioPlato p61) 20)
        (= (precioPlato p62) 20)
        (= (precioPlato p71) 20)
        (= (precioPlato p72) 5)
        (= (precioPlato p81) 20)
        (= (precioPlato p82) 20)
        (= (precioPlato p91) 20)
        (= (precioPlato p92) 20)
    )

    (:goal  (and 
                (forall (?s - slot) (slotOcupado ?s))
                (forall (?p - plato)
                    (imply (platoObligado ?p)
                        (platoAsignado ?p)
                    )
                )
            )
    )
    
    (:metric minimize (total-cost))
)