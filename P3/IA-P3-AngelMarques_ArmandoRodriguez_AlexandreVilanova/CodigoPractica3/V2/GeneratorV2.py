import itertools
import random
import time

def level(n):
    return "\t"*n

def endl():
    return "\n"

OUT_FILE = "problem_{time}.pddl".format(time=int(time.time()))
N_PLATOS = random.randint(10, 25)
N_INCOMP = random.randint(int(N_PLATOS/4), int(N_PLATOS/2))
N_DIAS   = 5
N_SLOTS  = 2
N_TIPOS = random.randint(int(N_PLATOS/4), int(N_PLATOS/2))

PLATOS  = ["p{n}".format(n=i) for i in range(N_PLATOS)]
DIAS    = ["d{n}".format(n=i) for i in range(N_DIAS)]
SLOTS   = ["s{n}".format(n=i) for i in range(N_DIAS*N_SLOTS)]
ORDENES = ["o{n}".format(n=i) for i in range(N_SLOTS)]
TIPOS   = ["t{n}".format(n=i) for i in range(N_TIPOS)]

output = ""
# --------------------------------------------------------------------------------------- 
output += level(0) + "(define (problem ProblemV2Generated)" + endl()
# ---------------------------------------------------------------------------------------
output += level(1) + "(:domain RicoRico)" + endl()
# ---------------------------------------------------------------------------------------
output += level(1) + "(:objects" + endl()
output += level(2) + " ".join(PLATOS) + " - plato" + endl()
output += level(2) + " ".join(DIAS) + " - dia" + endl()
output += level(2) + " ".join(SLOTS) + " - slot" + endl()
output += level(2) + " ".join(ORDENES) + " - orden" + endl()
output += level(2) + " ".join(TIPOS) + " - tipo" + endl()
output += level(1) + ")" + endl()
# ---------------------------------------------------------------------------------------
output += level(1) + "(:init" + endl()
orderList = random.sample(range(N_PLATOS), N_PLATOS)
for i in range(N_PLATOS):
    nombrePlato = PLATOS[i]
    nombreOrden = ORDENES[orderList[i]%N_SLOTS]
    output += level(2) + "(ordenPlato {plato} {orden})".format(plato=nombrePlato, orden=nombreOrden) + endl()
for i, name in enumerate(PLATOS):
    indexTipo = random.randrange(N_TIPOS)
    output += level(2) + "(tipoPlato {plato} {tipo})".format(plato=name, tipo=TIPOS[indexTipo]) + endl()
for i, name in enumerate(SLOTS):
    output += level(2) + "(ordenSlot {slot} {orden})".format(slot=name, orden=ORDENES[i%N_SLOTS]) + endl()
for i, name in enumerate(SLOTS):
    output += level(2) + "(diaSlot {slot} {dia})".format(slot=name, dia=DIAS[i%N_DIAS]) + endl()
for i in range(N_DIAS-1):
    output += level(2) + "(diaContiguo {d1} {d2})".format(d1=DIAS[i], d2=DIAS[i+1]) + endl()
    output += level(2) + "(diaContiguo {d1} {d2})".format(d1=DIAS[i+1], d2=DIAS[i]) + endl()
combinacionesPlatos = list(itertools.combinations(range(N_PLATOS), 2))
for i in range(N_INCOMP):
    indexCom = random.randrange(len(combinacionesPlatos))
    inc = combinacionesPlatos.pop(indexCom)
    output += level(2) + "(incompatible {pA} {pB})".format(pA=PLATOS[inc[0]], pB=PLATOS[inc[1]]) + endl()
    output += level(2) + "(incompatible {pA} {pB})".format(pA=PLATOS[inc[1]], pB=PLATOS[inc[0]]) + endl()
output += level(1) + ")" + endl()
# ---------------------------------------------------------------------------------------
output += level(1) + "(:goal (forall (?s - slot) (slotOcupado ?s)))" + endl()
# ---------------------------------------------------------------------------------------
output += level(0) + ")"
# ---------------------------------------------------------------------------------------
# print(output)

text_file = open(OUT_FILE, "w")
text_file.write(output)
text_file.close()
