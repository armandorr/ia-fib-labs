(define (problem ProblemV2Generated)
	(:domain RicoRico)
	(:objects
		p0 p1 p2 p3 p4 p5 p6 p7 p8 p9 p10 p11 p12 p13 p14 p15 p16 p17 - plato
		d0 d1 d2 d3 d4 - dia
		s0 s1 s2 s3 s4 s5 s6 s7 s8 s9 - slot
		o0 o1 - orden
		t0 t1 t2 t3 t4 t5 - tipo
	)
	(:init
		(ordenPlato p0 o0)
		(ordenPlato p1 o1)
		(ordenPlato p2 o1)
		(ordenPlato p3 o0)
		(ordenPlato p4 o0)
		(ordenPlato p5 o1)
		(ordenPlato p6 o1)
		(ordenPlato p7 o0)
		(ordenPlato p8 o0)
		(ordenPlato p9 o1)
		(ordenPlato p10 o1)
		(ordenPlato p11 o0)
		(ordenPlato p12 o1)
		(ordenPlato p13 o1)
		(ordenPlato p14 o0)
		(ordenPlato p15 o1)
		(ordenPlato p16 o0)
		(ordenPlato p17 o0)
		(tipoPlato p0 t0)
		(tipoPlato p1 t0)
		(tipoPlato p2 t4)
		(tipoPlato p3 t3)
		(tipoPlato p4 t5)
		(tipoPlato p5 t2)
		(tipoPlato p6 t3)
		(tipoPlato p7 t1)
		(tipoPlato p8 t2)
		(tipoPlato p9 t0)
		(tipoPlato p10 t4)
		(tipoPlato p11 t0)
		(tipoPlato p12 t3)
		(tipoPlato p13 t0)
		(tipoPlato p14 t2)
		(tipoPlato p15 t1)
		(tipoPlato p16 t0)
		(tipoPlato p17 t3)
		(ordenSlot s0 o0)
		(ordenSlot s1 o1)
		(ordenSlot s2 o0)
		(ordenSlot s3 o1)
		(ordenSlot s4 o0)
		(ordenSlot s5 o1)
		(ordenSlot s6 o0)
		(ordenSlot s7 o1)
		(ordenSlot s8 o0)
		(ordenSlot s9 o1)
		(diaSlot s0 d0)
		(diaSlot s1 d1)
		(diaSlot s2 d2)
		(diaSlot s3 d3)
		(diaSlot s4 d4)
		(diaSlot s5 d0)
		(diaSlot s6 d1)
		(diaSlot s7 d2)
		(diaSlot s8 d3)
		(diaSlot s9 d4)
		(diaContiguo d0 d1)
		(diaContiguo d1 d0)
		(diaContiguo d1 d2)
		(diaContiguo d2 d1)
		(diaContiguo d2 d3)
		(diaContiguo d3 d2)
		(diaContiguo d3 d4)
		(diaContiguo d4 d3)
		(incompatible p6 p12)
		(incompatible p12 p6)
		(incompatible p0 p3)
		(incompatible p3 p0)
		(incompatible p13 p14)
		(incompatible p14 p13)
		(incompatible p0 p1)
		(incompatible p1 p0)
		(incompatible p10 p12)
		(incompatible p12 p10)
		(incompatible p2 p16)
		(incompatible p16 p2)
		(incompatible p14 p16)
		(incompatible p16 p14)
		(incompatible p11 p14)
		(incompatible p14 p11)
	)
	(:goal (forall (?s - slot) (slotOcupado ?s)))
)