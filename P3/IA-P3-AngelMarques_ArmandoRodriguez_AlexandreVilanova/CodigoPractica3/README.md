# IAPractica3pddl

## Generadores

Para usar los generadores es necesario tener instalado python3.

El comando a ejecutar es:

```
python3 <GeneradorVX.py>
```

Despúes de ejecutar este comando se tendría que crear un fichero con el problema generado, su nombre será `problem_TIME.pddl`, donde `TIME` será el timestamp de creación. 

## Planificadores

Para ejecutar los planificadores se tiene que usar el siguiente comando:

```
./ff -o <DomainVX.pddl> -f <ProblemVX.pddl>
```

En el caso de la versión 5 se tiene que añadir el flag `-O` para utilizar las optimizaciones:

```
./ff -o <DomainVX.pddl> -f <ProblemVX.pddl> -O
```
