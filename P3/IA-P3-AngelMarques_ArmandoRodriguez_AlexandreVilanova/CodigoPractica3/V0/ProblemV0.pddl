(define (problem ProblemV0) 
    (:domain RicoRico)

    (:objects p11 p12 p21 p22 p31 p32 p41 p42 p51 p52 p61 p62 p71 p72 p81 p82 p91 p92 - plato
              d1 d2 d3 d4 d5 - dia
              s1 s2 s3 s4 s5 s6 s7 s8 s9 s10 - slot
              o1 o2 - orden
    )

    (:init
        (ordenPlato p11 o1)
        (ordenPlato p12 o2)
        (ordenPlato p21 o1)
        (ordenPlato p22 o2)
        (ordenPlato p31 o1)
        (ordenPlato p32 o2)
        (ordenPlato p41 o1)
        (ordenPlato p42 o2)
        (ordenPlato p51 o1)
        (ordenPlato p52 o2)
        (ordenPlato p61 o1)
        (ordenPlato p62 o2)
        (ordenPlato p71 o1)
        (ordenPlato p72 o2)
        (ordenPlato p81 o1)
        (ordenPlato p82 o2)
        (ordenPlato p91 o1)
        (ordenPlato p92 o2)
        
        (ordenSlot s1 o1)
        (ordenSlot s2 o2)
        (diaSlot s1 d1)
        (diaSlot s2 d1)
        
        (ordenSlot s3 o1)
        (ordenSlot s4 o2)
        (diaSlot s3 d2)
        (diaSlot s4 d2)
        
        (ordenSlot s5 o1)
        (ordenSlot s6 o2)
        (diaSlot s5 d3)
        (diaSlot s6 d3)
        
        (ordenSlot s7 o1)
        (ordenSlot s8 o2)
        (diaSlot s7 d4)
        (diaSlot s8 d4)
        
        (ordenSlot s9 o1)
        (ordenSlot s10 o2)
        (diaSlot s9 d5)
        (diaSlot s10 d5)
        
        (incompatible p11 p12)
        (incompatible p21 p22)
        (incompatible p31 p32)
        (incompatible p41 p42)
        (incompatible p51 p52)
        (incompatible p61 p62)
        (incompatible p71 p72)
        (incompatible p81 p82)
        (incompatible p91 p92)
    )

    (:goal (forall (?s - slot) (slotOcupado ?s)))
)