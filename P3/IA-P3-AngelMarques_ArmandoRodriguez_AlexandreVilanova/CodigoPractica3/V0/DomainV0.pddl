(define (domain RicoRico)

    (:requirements :adl :typing)

    (:types 
        plato dia orden slot - object
    )
        
    (:predicates
        (diaSlot ?s - slot ?dia - dia) ;; slot ?S pertenece al dia ?dia
        (ordenSlot ?s - slot ?orden - orden) ;; slot ?s tiene orden ?orden
        (slotOcupado ?s - slot) ;; slot ?s ocupado
        (ordenPlato ?plato - plato ?orden - orden) ;; ?plato tiene orden ?orden
        (incompatible ?plato1 - plato ?plato2 - plato) ;; ?plato1 incompatible con ?plato2
        (platoEnSlot ?plato - plato ?s - slot) ;; ?plato esta en el slot ?s asignado
    )

    (:action asignarPlatoDiaSlot
        :parameters (?plato - plato ?dia - dia ?slot - slot ?orden - orden)
        :precondition  (and (not (slotOcupado ?slot)) 
                            (diaSlot ?slot ?dia) 
                            (ordenPlato ?plato ?orden)
                            (ordenSlot ?slot ?orden)

                            (forall (?s - slot)
                                (imply (and (not (ordenSlot ?s ?orden)) (slotOcupado ?s) (diaSlot ?s ?dia))
                                    (exists (?p - plato)
                                        (and (platoEnSlot ?p ?s)
                                            (and (not (incompatible ?plato ?p))
                                                (not (incompatible ?p ?plato))
                                            )
                                        )
                                    )
                                )
                            )
                      )
        :effect (and (slotOcupado ?slot) (platoEnSlot ?plato ?slot))
    )
)
