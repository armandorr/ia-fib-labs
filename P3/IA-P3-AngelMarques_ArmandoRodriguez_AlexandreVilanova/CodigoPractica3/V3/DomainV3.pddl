(define (domain RicoRico)

    (:requirements :adl :typing)

    (:types 
        plato tipo dia orden slot - object
    )
        
    (:predicates 
        (diaSlot ?s - slot ?dia - dia) ;; slot ?S pertenece al dia ?dia
        (ordenSlot ?s - slot ?orden - orden) ;; slot ?s tiene orden ?orden
        (slotOcupado ?s - slot) ;; slot ?s ocupado
        (ordenPlato ?plato - plato ?orden - orden) ;; ?plato tiene orden ?orden
        (incompatible ?plato1 - plato ?plato2 - plato) ;; ?plato1 incompatible con ?plato2
        (platoEnSlot ?plato - plato ?s - slot) ;; ?plato esta en el slot ?s asignado
        (platoAsignado ?plato - plato) ;; ?plato ha sido asignado
        (tipoPlato ?plato - plato ?tipo - tipo) ;; ?plato es del tipo ?tipo
        (diaContiguo ?dia1 ?dia2 - dia) ;; ?dia1 es contiguo a ?dia2
        
        ;; Addition
        (platoObligado ?plato - plato) ;; el plato ?plato es un plato obligatorio (ha de aparecer en el menu semanal)
        (platoObligadoDia ?plato - plato ?dia - dia) ;; el plato ?plato ha de aparecer en el menu del dia ?dia
    )

    (:action asignarPlatoDiaSlot
        :parameters (?platoAnadir - plato 
                     ?slotAnadir - slot 
                     ?diaAnadir - dia 
                     ?tipo - tipo 
                     ?orden - orden)
        :precondition   (and 
                            (not (platoAsignado ?platoAnadir))
                            (not (slotOcupado ?slotAnadir))
                            (tipoPlato ?platoAnadir ?tipo)
                            (ordenPlato ?platoAnadir ?orden)
                            (diaSlot ?slotAnadir ?diaAnadir)
                            (ordenSlot ?slotAnadir ?orden)
                            (imply (platoObligado ?platoAnadir)
                                (platoObligadoDia ?platoAnadir ?diaAnadir)
                            )
                            (forall (?s - slot)
                                (imply (slotOcupado ?s)
                                    (and 
                                        (imply (and (not (ordenSlot ?s ?orden))(diaSlot ?s ?diaAnadir))
                                            (exists (?p - plato)
                                                (and (platoEnSlot ?p ?s)
                                                    (and (not (incompatible ?platoAnadir ?p))
                                                        (not (incompatible ?p ?platoAnadir))
                                                    )
                                                )
                                            )
                                        )
                                        (forall (?d - dia)
                                            (imply (and (ordenSlot ?s ?orden)(diaContiguo ?diaAnadir ?d)(diaSlot ?s ?d))
                                                (exists (?p - plato)
                                                    (and (platoEnSlot ?p ?s)
                                                        (not (tipoPlato ?p ?tipo))
                                                    )
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
        :effect (and 
                    (slotOcupado ?slotAnadir)
                    (platoEnSlot ?platoAnadir ?slotAnadir)
                    (platoAsignado ?platoAnadir)
                )
    )
)