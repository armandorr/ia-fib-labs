# IA-P1-RedSensores

## Execute
Run ```make compile``` in order to compile the project. After compiling you can use ```make run``` to run the project.

Alternatively you can execute the provided jars compiled by us using ```make runjar11``` or ```make runjar15```.

They are compiled with java version "11.0.10" and "15.0.2" respectively.

Warning: Using a different version might produce errors in the execution.

Created by: Angel Marques, Armando Rodriguez y Alexandre Vilanova