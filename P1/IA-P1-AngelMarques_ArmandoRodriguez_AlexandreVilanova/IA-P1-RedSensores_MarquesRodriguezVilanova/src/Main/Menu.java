package Main;

import Clases.Heuristics.*;
import Clases.States.*;
import Clases.Successors.*;
import aima.search.framework.*;
import aima.search.informed.HillClimbingSearch;
import aima.search.informed.SimulatedAnnealingSearch;

import javax.swing.*;
import java.awt.*;
import java.util.Random;
import java.util.Scanner;

public class Menu
{
    private static int numSensors;
    private static int numCenters;
    private static int seedSensors;
    private static int seedCenters;
    private static int seedRandomInitial;
    private static boolean displayGraph;

    enum searchAlgorithm { HILL_CLIMBING, SIMULATED_ANNEALING }
    private static searchAlgorithm searchAlgorithmChosen;

    enum initialState { SENSORS, RANDOM }
    private static initialState initialStateChosen;

    enum heuristic { TOTAL, DATA }
    private static heuristic heuristicChosen;

    enum operator { MOVE, CHANGE, MOVE_CHANGE }
    private static operator operatorChosen;

    public static void main(String[] args) {
        System.out.println("Practica 1: Red de distribución sensores/centros IA-20/21 ");

        Scanner scanner = new Scanner(System.in);
        int answer = -1;
        while (answer < 0 || answer > 1) {
            System.out.println("DEFAULT[0] / EXPERIMENTATION[1]");
            answer = scanner.nextInt();
        }

        if(answer == 0)
        {
            dataInput(false);
            initialInformation(false);
            localSearch();
        }
        else {
            dataInput(true);
            System.out.println("Enter the size of the experiment:");
            int experimentSize = scanner.nextInt();
            System.out.println("Enter a seed for the experiment generation:");
            int experimentSeed = scanner.nextInt();
            initialInformation(true);
            localSearchExperiment(experimentSize, experimentSeed);
        }
        System.out.println("\nDone by Angel Marqués, Armando Rodriguez and Alexandre Vilanova.");
    }

    public static void dataInput(boolean experiment)
    {
        Scanner scanner = new Scanner(System.in);
        int answer = 0;
        while (answer < 1 || answer > 2)
        {
            System.out.println("Enter the desired local search algorithm: Hill Climbing[1] or Simulated Annealing[2]");
            answer = scanner.nextInt();
            if(answer == 1) searchAlgorithmChosen = searchAlgorithm.HILL_CLIMBING;
            else if(answer == 2) searchAlgorithmChosen = searchAlgorithm.SIMULATED_ANNEALING;
        }

        if(!experiment)
        {
            System.out.println("Enter a seed for the sensor data generation:");
            seedSensors = scanner.nextInt();
            System.out.println("Enter a seed for the center data generation:");
            seedCenters = scanner.nextInt();
        }

        while (numSensors < 1)
        {
            System.out.println("Enter the desired number of sensors:");
            numSensors = scanner.nextInt();
        }

        while (numCenters < 1)
        {
            System.out.println("Enter the desired number of centers:");
            numCenters = scanner.nextInt();
        }

        answer = 0;
        while (answer < 1 || answer > 2)
        {
            System.out.println("Choose the desired initial state:");
            System.out.println("1: Sensors");
            System.out.println("2: Random");
            answer = scanner.nextInt();
            if(answer == 1) initialStateChosen = initialState.SENSORS;
            else if(answer == 2) initialStateChosen = initialState.RANDOM;
        }

        if (initialStateChosen == initialState.RANDOM)
        {
            System.out.println("Enter a seed for the random initial state:");
            seedRandomInitial = scanner.nextInt();
        }

        answer = 0;
        while (answer < 1 || answer > 2)
        {
            System.out.println("Choose the desired heuristic function:");
            System.out.println("1: Total");
            System.out.println("2: Data");
            answer = scanner.nextInt();
            if(answer == 1) heuristicChosen = heuristic.TOTAL;
            else if(answer == 2) heuristicChosen = heuristic.DATA;
        }

        if(searchAlgorithmChosen == searchAlgorithm.HILL_CLIMBING)
        {
            answer = 0;
            while (answer < 1 || answer > 3) {
                System.out.println("Choose the desired operator:");
                System.out.println("1: Move connection");
                System.out.println("2: Change connection");
                System.out.println("3: Move and change connection");
                answer = scanner.nextInt();
                if (answer == 1) operatorChosen = operator.MOVE;
                else if (answer == 2) operatorChosen = operator.CHANGE;
                else if (answer == 3) operatorChosen = operator.MOVE_CHANGE;
            }
        }
        
        if(!experiment)
        {
            System.out.println("Do you want to display the visual graphs? Yes[1]/No[0]");
            System.out.println("WARNING: It may produce an error when executing it on Windows");
            displayGraph = scanner.nextInt() == 1;
        }
    }

    public static void initialInformation(boolean experiment) {
        System.out.println("------------------Initialization information------------------");
        System.out.println();

        System.out.println("    Local search algorithm:  "+ searchAlgorithmChosen);

        System.out.println("    Initial state:           "+ initialStateChosen);

        System.out.println("    Heuristic:               "+ heuristicChosen);

        if(searchAlgorithmChosen == searchAlgorithm.HILL_CLIMBING)
            System.out.println("    Operator:                "+ operatorChosen + " connection");

        System.out.println();
        if(!experiment)
        {
            System.out.println("    Seed Centers:            " + seedCenters);
            System.out.println("    Seed Sensors:            " + seedSensors);
        }
        System.out.println("    Number of centers:       "+numCenters);
        System.out.println("    Number of sensors:       "+numSensors);
        System.out.println("\n");
    }

    public static void localSearch() {
        RedSensorsState firstState = new RedSensorsState(numSensors, seedSensors, numCenters, seedCenters);

        if (initialStateChosen == initialState.SENSORS)
            firstState.generateInitialStateSensorsFirst();
        else if (initialStateChosen == initialState.RANDOM)
            firstState.generateInitialStateRandom(seedRandomInitial);

        System.out.println("-------------------Initial state information-----------------\n");
        System.out.println("    Total connection cost: " + firstState.getTotalConnectionCost());
        System.out.println("    Data volume received:  " + (firstState.getTotalDataAtCenters()/firstState.getTotalDataWithAllSensors())*100 + "%\n");
        System.out.println();

        if(displayGraph) printGraph(firstState,"Initial state plot");

        SuccessorFunction successorFunction = null;
        if(searchAlgorithmChosen == searchAlgorithm.SIMULATED_ANNEALING)
            successorFunction = new SuccessorFunctionSimulatedAnnealing();
        else{
            if (operatorChosen == operator.MOVE)
                successorFunction = new SuccessorFunctionMoveConnection();
            else if (operatorChosen == operator.CHANGE)
                successorFunction  = new SuccessorFunctionChangeConnection();
            else if (operatorChosen == operator.MOVE_CHANGE)
                successorFunction  = new SuccessorFunctionMoveAndChangeConnection();
        }

        HeuristicFunction heuristicFunction = null;
        if (heuristicChosen == heuristic.TOTAL)
            heuristicFunction = new HeuristicFunctionTotalCost();
        else if (heuristicChosen == heuristic.DATA)
            heuristicFunction = new HeuristicFunctionDataVolume();

        Problem problem = new Problem(firstState, successorFunction, new RedSensorsGoalTest(), heuristicFunction);

        try {
            Search search;
            if(searchAlgorithmChosen == searchAlgorithm.SIMULATED_ANNEALING)
                search = new SimulatedAnnealingSearch(35000, 10, 50, 0.01d);
            else search = new HillClimbingSearch();

            long startTime = System.nanoTime();

            new SearchAgent(problem, search);

            long endTime = System.nanoTime();

            RedSensorsState finalState = (RedSensorsState) search.getGoalState();

            System.out.println("-------------------Final state information--------------------\n");
            System.out.println("    Total connection cost: " + finalState.getTotalConnectionCost());
            System.out.println("    Data volume received:  " + (finalState.getTotalDataAtCenters()/finalState.getTotalDataWithAllSensors())*100 + "%");
            System.out.println("    Execution time:        " + (endTime-startTime)/1000000000.0 + " seconds\n");
            System.out.println("--------------------------------------------------------------");

            if(displayGraph) printGraph(finalState,"Final state plot");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void localSearchExperiment(int size, int seed) {
        double[] costEachExecution = new double[size];
        double[] timeEachExecution = new double[size];
        double[] percentageEachExecution = new double[size];
        double[] numCentersUsedEachExecution = new double[size];

        Random random = new Random(seed);
        double connectionCost = 0;
        double percentage = 0;
        double time = 0;
        double numDataCentersUsed = 0;

        for (int numIt = 0; numIt < size; ++numIt)
        {
            seedSensors = random.nextInt();
            seedCenters = random.nextInt();

            RedSensorsState firstState = new RedSensorsState(numSensors, seedSensors, numCenters, seedCenters);

            if (initialStateChosen == initialState.SENSORS)
                firstState.generateInitialStateSensorsFirst();
            else if (initialStateChosen == initialState.RANDOM)
                firstState.generateInitialStateRandom(seedRandomInitial);

            SuccessorFunction successorFunction = null;
            if (searchAlgorithmChosen == searchAlgorithm.SIMULATED_ANNEALING)
                successorFunction = new SuccessorFunctionSimulatedAnnealing();
            else {
                if (operatorChosen == operator.MOVE)
                    successorFunction = new SuccessorFunctionMoveConnection();
                else if (operatorChosen == operator.CHANGE)
                    successorFunction = new SuccessorFunctionChangeConnection();
                else if (operatorChosen == operator.MOVE_CHANGE)
                    successorFunction = new SuccessorFunctionMoveAndChangeConnection();
            }

            HeuristicFunction heuristicFunction = null;
            if (heuristicChosen == heuristic.TOTAL)
                heuristicFunction = new HeuristicFunctionTotalCost();
            else if (heuristicChosen == heuristic.DATA)
                heuristicFunction = new HeuristicFunctionDataVolume();

            Problem problem = new Problem(firstState, successorFunction, new RedSensorsGoalTest(), heuristicFunction);

            try {
                Search search;
                if (searchAlgorithmChosen == searchAlgorithm.SIMULATED_ANNEALING)
                    search = new SimulatedAnnealingSearch(35000, 10, 50, 0.01d);
                else search = new HillClimbingSearch();

                long startTime = System.nanoTime();

                new SearchAgent(problem, search);

                long endTime = System.nanoTime();

                RedSensorsState finalState = (RedSensorsState) search.getGoalState();

                double actualConnectionCost = finalState.getTotalConnectionCost();
                costEachExecution[numIt] = actualConnectionCost;
                connectionCost += actualConnectionCost;

                double actualPercentage = (finalState.getTotalDataAtCenters() / finalState.getTotalDataWithAllSensors()) * 100;
                percentageEachExecution[numIt] = actualPercentage;
                percentage += actualPercentage;

                int actualNumDataCentersUsed = finalState.getNumDataCentersUsed();
                numCentersUsedEachExecution[numIt] = actualNumDataCentersUsed;
                numDataCentersUsed += actualNumDataCentersUsed;

                double actualTime = (endTime - startTime) / 1000000000.0;
                timeEachExecution[numIt] = actualTime;
                time += actualTime;

                System.out.println("Execution " + (numIt+1) + " -----------------------------------");
                System.out.println("|  Connection cost: " + actualConnectionCost);
                System.out.println("|  Data volume received: " + actualPercentage + "%");
                System.out.println("|  Num centers used: " + actualNumDataCentersUsed);
                System.out.println("|  Execution time: " + actualTime + " seconds");
                System.out.println("-----------------------------------------------\n");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        System.out.println("----------------RESULTS WITH " + size + " ITERATIONS-----------------\n");
        System.out.println("Average connection cost: " + connectionCost / size);
        System.out.println("Average data volume percentage: " + percentage / size + "%");
        System.out.println("Average data centers used: " + numDataCentersUsed / size);
        System.out.println("Average execution time: " + time / size + " seconds\n");

        System.out.println("Standard deviation connection cost: " + calculateDesv(costEachExecution,connectionCost / size));
        System.out.println("Standard deviation data volume percentage: " + calculateDesv(percentageEachExecution,percentage / size) + "%");
        System.out.println("Standard deviation data centers used: " + calculateDesv(numCentersUsedEachExecution,numDataCentersUsed / size));
        System.out.println("Standard deviation time: " + calculateDesv(timeEachExecution,time / size) + " seconds\n");
        System.out.println("----------------------------------------------------------");
    }

    private static double calculateDesv(double[] values, double media)
    {
        double totalSum = 0;
        for (double value : values)
        {
            totalSum += Math.pow((value - media), 2);
        }
        return Math.sqrt(totalSum/values.length);
    }

    public static void printGraph(RedSensorsState initialState, String windowName){
        Frame f = new SimpleGraph(initialState,windowName);
        f.setVisible(true);
    }

    private static class SimpleGraph extends JFrame {
        private final RedSensorsState state;

        public SimpleGraph(RedSensorsState initialState, String windowName) {
            super(windowName);
            state = initialState;
            Container drawable = getContentPane();
            JPanel canvas = new GraphCanvas();
            drawable.add(canvas);
            setSize(850, 850);
        }

        public class GraphCanvas extends JPanel {
            public GraphCanvas() {}

            public void paint(Graphics g) {
                Graphics2D g2 = (Graphics2D) g;
                g2.setFont(new Font("Monaco", Font.BOLD, 20));
                String text = "Connection cost: "+(int)state.getTotalConnectionCost() + "  Proportion data received: "+ Math.floor((state.getTotalDataAtCenters()/state.getTotalDataWithAllSensors())*100 * 100) / 100 +"  Total cost: "+Math.floor(state.getTotalCost() * 100) / 100;
                g2.drawString(text,25,25);
                g2.setFont(new Font("Monaco", Font.PLAIN, 15));
                g2.setColor(Color.BLUE);
                int tamOval = 10;
                int r = tamOval /2;
                int separationX = 35;
                int separationY = 50;
                float fact = 7.5f;
                for(int i = 0; i < numSensors; ++i){
                    int posX = state.getPositionSensorX(i);
                    int posY = state.getPositionSensorY(i);
                    g2.fillOval((int)(separationX + fact*posX -r), (int)(separationY + fact*posY -r), tamOval, tamOval);
                    String capacity = state.getDataVolumeTransmitted(i) +"/"+3*state.getCapturedData(i);
                    g2.drawString(capacity, (int)(separationX + fact*posX-r), (int)(separationY + fact*posY -r));
                }
                g2.setColor(Color.RED);
                int tamOvalCenter = 16;
                int rCenter = tamOvalCenter /2;
                for(int i = 0; i < numCenters; ++i){
                    int posX = state.getPositionCenterX(i);
                    int posY = state.getPositionCenterY(i);
                    g2.fillOval((int)(separationX + fact*posX -rCenter), (int)(separationY + fact*posY -rCenter), tamOvalCenter, tamOvalCenter);
                    String capacity = (150 - state.getRealFreeStorage(i + numSensors)) +"/150";
                    g2.drawString(capacity, (int)(separationX + fact*posX -rCenter), (int)(separationY + fact*posY -rCenter));
                }
                g2.setColor(Color.BLACK);
                for(int i = 0; i < numSensors; ++i){
                    int connection = state.getConnectionID(i);
                    if(connection != -1) {
                        int posX = state.getPositionSensorX(i);
                        int posY = state.getPositionSensorY(i);
                        int posXDest;
                        int posYDest;
                        if(connection < numSensors) {
                            posXDest = state.getPositionSensorX(connection);
                            posYDest = state.getPositionSensorY(connection);
                        }
                        else{
                            posXDest = state.getPositionCenterX(connection-numSensors);
                            posYDest = state.getPositionCenterY(connection-numSensors);
                        }
                        g2.drawLine((int)(separationX + fact*posX), (int)(separationY + fact*posY), (int)(separationX + fact*posXDest), (int)(separationY + fact*posYDest));
                    }
                }
            }
        }
    }
}
