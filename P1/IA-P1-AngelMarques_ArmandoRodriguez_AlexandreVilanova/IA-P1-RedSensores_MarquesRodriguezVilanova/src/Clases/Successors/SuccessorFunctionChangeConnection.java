package Clases.Successors;

import java.util.ArrayList;

import Clases.States.RedSensorsState;
import aima.search.framework.Successor;
import aima.search.framework.SuccessorFunction;

public class SuccessorFunctionChangeConnection implements SuccessorFunction
{
    public ArrayList<Successor> getSuccessors(Object state)
    {
        RedSensorsState actualState = (RedSensorsState) state;
        ArrayList<Successor> sons = new ArrayList<>();
        for(int i = 0; i < actualState.getSensorsSize(); ++i)
        {
            for (int j = 0; j < actualState.getSensorsSize(); ++j)
            {
                RedSensorsState newSonState = new RedSensorsState(actualState);
                if (newSonState.canChangeConnection(i,j))
                {
                    newSonState.changeConnection(i,j);
                    String connectionName = "Change connection sensor " + i + " to endPoint " + j + "\n";
                    sons.add(new Successor(connectionName, newSonState));
                }
            }
        }
        return sons;
    }
}
