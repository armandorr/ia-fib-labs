package Clases.Successors;

import Clases.States.RedSensorsState;
import aima.search.framework.Successor;
import aima.search.framework.SuccessorFunction;

import java.util.ArrayList;
import java.util.Random;

public class SuccessorFunctionSimulatedAnnealing implements SuccessorFunction
{
    public ArrayList<Successor> getSuccessors(Object state)
    {
        RedSensorsState actualState = (RedSensorsState) state;
        RedSensorsState newSonState = new RedSensorsState(actualState);
        ArrayList<Successor> sons = new ArrayList<>();
        Random rand = new Random();
        while(sons.size() == 0)
        {
            int i = rand.nextInt(actualState.getSensorsSize());
            int j = rand.nextInt(actualState.getSensorsAndCentersSize());
            if(rand.nextInt(2) == 1) //move connection
            {
                if (newSonState.canMoveConnection(i, j))
                {
                    newSonState.moveConnection(i, j);
                    String connectionName = "Move connection sensor " + i + " to endPoint " + j + "\n";
                    sons.add(new Successor(connectionName, newSonState));
                }
            }
            else //change connection
            {
                if (newSonState.canChangeConnection(i, j))
                {
                    newSonState.changeConnection(i, j);
                    String connectionName = "Change connection sensor " + i + " with sensor " + j + "\n";
                    sons.add(new Successor(connectionName, newSonState));
                }
            }
        }
        return sons;
    }
}
