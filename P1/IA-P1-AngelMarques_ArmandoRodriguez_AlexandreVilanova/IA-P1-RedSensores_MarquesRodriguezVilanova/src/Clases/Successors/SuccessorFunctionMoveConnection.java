package Clases.Successors;

import Clases.States.RedSensorsState;
import aima.search.framework.Successor;
import aima.search.framework.SuccessorFunction;

import java.util.ArrayList;

public class SuccessorFunctionMoveConnection implements SuccessorFunction
{
    public ArrayList<Successor> getSuccessors(Object state)
    {
        RedSensorsState actualState = (RedSensorsState) state;
        ArrayList<Successor> sons = new ArrayList<>();
        for(int i = 0; i < actualState.getSensorsSize(); ++i)
        {
            for (int j = 0; j < actualState.getSensorsAndCentersSize(); ++j)
            {
                RedSensorsState newSonState = new RedSensorsState(actualState);
                if (newSonState.canMoveConnection(i,j))
                {
                    newSonState.moveConnection(i, j);
                    String connectionName = "Move connection sensor " + i + " to endPoint " + j + "\n";
                    sons.add(new Successor(connectionName, newSonState));
                }
            }
        }
        return sons;
    }
}