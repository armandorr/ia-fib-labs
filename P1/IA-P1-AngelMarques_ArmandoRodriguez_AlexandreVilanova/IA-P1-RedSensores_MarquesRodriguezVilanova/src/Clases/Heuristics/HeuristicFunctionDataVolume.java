package Clases.Heuristics;

import Clases.States.RedSensorsState;
import aima.search.framework.HeuristicFunction;

public class HeuristicFunctionDataVolume implements HeuristicFunction
{
    public double getHeuristicValue(Object state)
    {
        return ((RedSensorsState) state).getCostDataVolume();
    }
}
