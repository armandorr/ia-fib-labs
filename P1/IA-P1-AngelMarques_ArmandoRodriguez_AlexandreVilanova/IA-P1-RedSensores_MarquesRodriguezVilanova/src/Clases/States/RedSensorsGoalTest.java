package Clases.States;

import aima.search.framework.GoalTest;

public class RedSensorsGoalTest implements GoalTest
{
    public boolean isGoalState(Object state)
    {
        return((RedSensorsState) state).isGoalState();
    }
}
