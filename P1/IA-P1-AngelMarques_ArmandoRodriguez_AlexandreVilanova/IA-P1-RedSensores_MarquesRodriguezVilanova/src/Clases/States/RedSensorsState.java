package Clases.States;

import IA.Red.Centro;
import IA.Red.CentrosDatos;
import IA.Red.Sensor;
import IA.Red.Sensores;

import java.util.Arrays;
import java.util.Random;

public class RedSensorsState {

    private static final int NOT_CONNECTED = -1;            // State not connected to represent the not connected sensors
    private static final int MAX_CAPACITY_CENTERS = 150;    // Maximum capacity of a center
    private static final int MAX_CONNECTIONS_CENTERS = 25;  // Maximum incoming connections of a center
    private static final int MAX_CONNECTIONS_SENSORS = 3;   // Maximum incoming connections of a center

    private static CentrosDatos dataCenters;                // Data center set
    private static Sensores sensors;                        // Sensor set
    private static int totalDataWithAllSensors;             // All the possible received data

    private final int[] numConnectionsLeft;                 // Num connections that can already get. Max in S=3,C=25. Size S + C.
    private final int[] connectionsID;                      // ConnectionID to know with who the sensor is connected. Size S
    private final int[] freeStorage;                        // Storage don't used. Max in S=2*capacity,C=150. Size S + C.


    private double totalConnectionCost;                     // Total connection cost of the network

    private int PONDERATION_COST_VALUE = 900000;            // Default ponderation value for the Total heuristic

    // State constructor
    public RedSensorsState(int numSensors, int seedSensors, int numCenters, int seedCenters)
    {
        sensors = new Sensores(numSensors,seedSensors);
        dataCenters = new CentrosDatos(numCenters,seedCenters);
        numConnectionsLeft = new int[numSensors+numCenters];
        connectionsID = new int[numSensors];
        freeStorage = new int[numSensors+numCenters];

        totalDataWithAllSensors = 0;

        for (int i = 0; i < numSensors+numCenters; ++i)
        {
            if(i < numSensors)//iterate sensors
            {
                numConnectionsLeft[i] = MAX_CONNECTIONS_SENSORS;
                connectionsID[i] = NOT_CONNECTED;

                int capacity = (int)sensors.get(i).getCapacidad();
                freeStorage[i] = 2*capacity;
                totalDataWithAllSensors += capacity;
            }
            else //iterate centers
            {
                numConnectionsLeft[i] = MAX_CONNECTIONS_CENTERS;
                freeStorage[i] = MAX_CAPACITY_CENTERS;
            }
        }
        totalConnectionCost = 0.0;
    }

    // State copy-constructor
    public RedSensorsState(RedSensorsState state)
    {
        this.numConnectionsLeft = (state.numConnectionsLeft).clone();
        this.connectionsID = (state.connectionsID).clone();
        this.freeStorage = (state.freeStorage).clone();
        this.totalConnectionCost = state.totalConnectionCost;
        this.PONDERATION_COST_VALUE = state.PONDERATION_COST_VALUE;
    }

    // Decrease the number of connections left of the identifier
    public void decreaseNumConnectionsLeft(int identifier)
    {
        --numConnectionsLeft[identifier];
    }

    // Increases the number of connections left of the identifier
    public void increaseNumConnectionsLeft(int identifier)
    {
        ++numConnectionsLeft[identifier];
    }

    // Pre: sourceID identifies a sensor
    // Returns the connection of sourceID
    public int getConnectionID(int sourceID)
    {
        return connectionsID[sourceID];
    }

    // Pre: sourceId is a sensor
    // Changes the connection of the sensor sourceID
    public void changeConnectionID(int sourceID, int connectedToID)
    {
        connectionsID[sourceID] = connectedToID;
    }

    // Get real free storage left of a identifier. If it is negative it returns 0.
    public int getRealFreeStorage(int identifier)
    {
        return Math.max(0,freeStorage[identifier]);
    }

    // Increases the available storage of endpoint identifer
    public void increaseFreeStorage(int identifier, int oldUsage)
    {
        freeStorage[identifier] += oldUsage;
    }

    // Decreases the available storage of endpoint identifer
    public void decreaseFreeStorage(int identifier, int newUsage)
    {
        freeStorage[identifier] -= newUsage;
    }

    // Pre: identifier is a sensor
    // Get the total data transmited by the sensor identifier
    public int getDataVolumeTransmitted(int identifier)
    {
        int capacity = (int)sensors.get(identifier).getCapacidad();
        return 3*capacity - getRealFreeStorage(identifier);
    }

    // Get number of centers of the current network
    public int getCentersSize()
    {
        return dataCenters.size();
    }

    // Get number of sensors of the current network
    public int getSensorsSize()
    {
        return sensors.size();
    }

    // Get number of centers and sensors of the current network
    public int getSensorsAndCentersSize()
    {
        return dataCenters.size() + sensors.size();
    }

    // Pre: sensorID is a sensor and sensorID != endPointID
    //     sensorID is connected to endPoint if oldDataTransmitted > 0
    //     sensorID is not connected to endPoint if oldDataTransmitted == 0
    // If oldDataTransmitted == 0 it means that we are making a connection and it can be done
    // If newDataTransmitted == 0 it means that we are making a disconnection
    // Function that updates the cost of the net with a change in the data volume transmitted between sensorID and endPoint
    private void updateConnectionCost(int sensorID, int endPointID, int oldDataTransmitted, int newDataTransmitted)
    {
        if(oldDataTransmitted != 0 || newDataTransmitted != 0) { //if the real change is 0 do nothing
            if (oldDataTransmitted == 0) { //connection
                changeConnectionID(sensorID, endPointID);
                decreaseNumConnectionsLeft(endPointID);
            } else if (newDataTransmitted == 0) { //disconnection
                changeConnectionID(sensorID, NOT_CONNECTED);
                increaseNumConnectionsLeft(endPointID);
            }

            if (endPointID < sensors.size()) { //endPoint is a sensor
                int oldDataTransmittedEndPoint = getDataVolumeTransmitted(endPointID);

                increaseFreeStorage(endPointID, oldDataTransmitted);
                decreaseFreeStorage(endPointID, newDataTransmitted);

                int newDataTransmittedEndPoint = getDataVolumeTransmitted(endPointID);

                int endPointConnectionID = connectionsID[endPointID];
                if (endPointConnectionID != NOT_CONNECTED) //end point is connected
                {
                    if(oldDataTransmittedEndPoint != newDataTransmittedEndPoint)
                        updateConnectionCost(endPointID, endPointConnectionID, oldDataTransmittedEndPoint, newDataTransmittedEndPoint);
                }
            }
            else //endPoint is a center
            {
                increaseFreeStorage(endPointID, oldDataTransmitted);
                decreaseFreeStorage(endPointID, newDataTransmitted);
            }

            Sensor startSensor = sensors.get(sensorID);
            double coordX, coordY;
            if (endPointID < sensors.size()) //SENSOR_TO_SENSOR
            {
                Sensor endSensor = sensors.get(endPointID);
                coordX = startSensor.getCoordX() - endSensor.getCoordX();
                coordY = startSensor.getCoordY() - endSensor.getCoordY();
            }
            else //SENSOR_TO_CENTER
            {
                Centro endDataCenter = dataCenters.get(endPointID-sensors.size());
                coordX = startSensor.getCoordX() - endDataCenter.getCoordX();
                coordY = startSensor.getCoordY() - endDataCenter.getCoordY();
            }

            double distance = Math.pow(coordX, 2) + Math.pow(coordY, 2);

            totalConnectionCost -= distance * oldDataTransmitted; //We reduce the previous data transmitted to the cost
            totalConnectionCost += distance * newDataTransmitted; //We increase the new data transmitted to the cost
        }
    }

    // Tell if there is a cycle involving sensor identifier
    private boolean thereIsCycle(int identifier, Boolean[] identifiersVisited)
    {
        if (identifier < sensors.size())
        {
            if(identifiersVisited[identifier]) return true;

            int connectionID = connectionsID[identifier];
            if (connectionID != NOT_CONNECTED)
            {
                identifiersVisited[identifier] = true;
                return thereIsCycle(connectionID, identifiersVisited);
            }
        }
        return false;
    }

    // Pre: sensorID is a sensor
    // Tell if we can make a connection between sensorID and endPointID
    public boolean canMakeConnection(int sensorID, int endPointID)
    {
        if(connectionsID[sensorID] == NOT_CONNECTED)
        {
            Boolean[] identifiersVisited = new Boolean[sensors.size()];
            Arrays.fill(identifiersVisited, false);
            identifiersVisited[sensorID] = true;
            if (!thereIsCycle(endPointID, identifiersVisited))
            {
                return numConnectionsLeft[endPointID] > 0;
            }
        }
        return false;
    }

    // Connects sensorID to endPointID
    public void makeConnection(int sensorID, int endPointID)
    {
        updateConnectionCost(sensorID, endPointID, 0,getDataVolumeTransmitted(sensorID));
    }

    // Pre: sensorID is a sensor
    // Tell if we can move the actual connection of sensorID to endPointID
    public boolean canMoveConnection(int sensorID, int endPointID)
    {
        if(connectionsID[sensorID] != NOT_CONNECTED)
        {
            Boolean[] identifiersVisited = new Boolean[sensors.size()];
            Arrays.fill(identifiersVisited, false);
            identifiersVisited[sensorID] = true;
            if (!thereIsCycle(endPointID, identifiersVisited))
            {
                return numConnectionsLeft[endPointID] > 0;
            }
        }
        return false;
    }

    // Moves the actual connection of sensorID to endPointID
    public void moveConnection(int sensorID, int endPointID)
    {
        updateConnectionCost(sensorID, connectionsID[sensorID], getDataVolumeTransmitted(sensorID),0);
        updateConnectionCost(sensorID, endPointID, 0,getDataVolumeTransmitted(sensorID));
    }

    // Pre: sensorID1 and sensorID2 are sensors
    // Tell if we can change the connections between both sensors
    // connecting the sensorID1 to the connection of the sensorID2 and vice versa
    public boolean canChangeConnection(int sensorID1, int sensorID2)
    {
        if(sensorID2 >= sensors.size()) return false;
        int connectionOfSensor1 = connectionsID[sensorID1];
        int connectionOfSensor2 = connectionsID[sensorID2];
        return canMoveConnection(sensorID1,connectionOfSensor2) && canMoveConnection(sensorID2,connectionOfSensor1);
    }

    // Pre: sensorID1 and sensorID2 are sensors
    // Change the connections of those two sensors
    public void changeConnection(int sensorID1, int sensorID2)
    {
        int connectionOfSensor1 = connectionsID[sensorID1];
        int connectionOfSensor2 = connectionsID[sensorID2];
        moveConnection(sensorID1,connectionOfSensor2);
        moveConnection(sensorID2,connectionOfSensor1);
    }

    // Initial State: SENSORS
    // Connects sensor0 to sensor1, sensor1 to sensor2, etc ... until sensorN, which is connected to datacenter0
    public void generateInitialStateSensorsFirst() {
        int sizeSensors = sensors.size();
        int sizeCenters = dataCenters.size();
        for (int i = 0; i < sizeSensors; ++i)
        {
            boolean connected = false;
            for (int j = i+1; j < sizeSensors+sizeCenters && !connected; ++j)
            {
                if (canMakeConnection(i,j)) {
                    makeConnection(i, j);
                    connected = true;
                }
            }
        }
    }

    // Initial State: RANDOM
    // Connects each sensor to a random endpoint
    public void generateInitialStateRandom(int num) {
        int sizeSensors = sensors.size();
        int sizeCenters = dataCenters.size();
        Random random = new Random(num);
        for (int originSensor = 0; originSensor < sizeSensors; ++originSensor) {
            boolean connected = false;
            while(!connected) {
                int dest = random.nextInt(sizeSensors+sizeCenters);
                if (canMakeConnection(originSensor, dest)) {
                    makeConnection(originSensor, dest);
                    connected = true;
                }
            }
        }
    }

    // Get the maximum data available that we could receive
    public double getTotalDataWithAllSensors()
    {
        return totalDataWithAllSensors;
    }

    // Get the number of data centers used (that have connections) in the network
    public int getNumDataCentersUsed()
    {
        int numDataCentersUsed = 0;
        int sizeSensors = sensors.size();
        for(int i = 0; i < dataCenters.size(); ++i){
            if(MAX_CAPACITY_CENTERS > getRealFreeStorage(sizeSensors+i))
                ++numDataCentersUsed;
        }
        return numDataCentersUsed;
    }

    // Heuristic DATA
    // Get the difference between all the transmitted and all that we can transmmit
    public double getCostDataVolume()
    {
        return totalDataWithAllSensors - getTotalDataAtCenters();
    }

    // Heuristic TOTAL
    // Get total connection cost taking into account the data sent ponderation
    public double getTotalCost()
    {
        return totalConnectionCost - (getTotalDataAtCenters()/totalDataWithAllSensors)*PONDERATION_COST_VALUE;
    }

    // Get the total connection cost
    public double getTotalConnectionCost()
    {
        return totalConnectionCost;
    }

    // Change the ponderation value
    public void setPonderationCostValue(int value)
    {
        PONDERATION_COST_VALUE = value;
    }

    // Get the amount of data that reaches data centers in the network
    public double getTotalDataAtCenters()
    {
        int sizeSensors = sensors.size();
        int totalDataAtCenters = 0;
        for(int i = 0; i < dataCenters.size(); ++i){
            totalDataAtCenters += MAX_CAPACITY_CENTERS - getRealFreeStorage(sizeSensors+i);
        }
        return totalDataAtCenters;
    }

    // Pre: i identifies a sensor
    // Get captured data by sensor i
    public int getCapturedData(int i)
    {
        return (int)sensors.get(i).getCapacidad();
    }

    // Pre: i identifies a sensor
    // Get coord x of sensor i
    public int getPositionSensorX(int i)
    {
        return sensors.get(i).getCoordX();
    }

    // Pre: i identifies a sensor
    // Get coord y of sensor i
    public int getPositionSensorY(int i)
    {
        return sensors.get(i).getCoordY();
    }

    // Pre: i identifies a center
    // Get coord x of center i
    public int getPositionCenterX(int i)
    {
        return dataCenters.get(i).getCoordX();
    }

    // Pre: i identifies a center
    // Get coord y of center i
    public int getPositionCenterY(int i)
    {
        return dataCenters.get(i).getCoordY();
    }

    // Tell if we are in a solution final state
    // It always returns false as we want to find better solutions if possible
    public boolean isGoalState()
    {
        return false;
    }
}
