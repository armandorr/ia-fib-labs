; Sat May 22 10:43:41 CEST 2021
; 
;+ (version "3.5")
;+ (build "Build 663")


(defclass %3ACLIPS_TOP_LEVEL_SLOT_CLASS "Fake class to save top-level slot information"
	(is-a USER)
	(role abstract)
	(multislot Plato_epocas
		(type SYMBOL)
		(allowed-values PRIMAVERA VERANO OTONO INVIERNO)
		(cardinality 1 ?VARIABLE)
		(create-accessor read-write))
	(single-slot Bebida_nombre
		(type STRING)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot Propiedad_nombre
		(type STRING)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot Plato_complejidad
		(type INTEGER)
		(range 1 3)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot Plato_estiloComida
		(type INSTANCE)
;+		(allowed-classes EstiloComida)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot Bebida_alcholica
		(type SYMBOL)
		(allowed-values FALSE TRUE)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(multislot Plato_propiedades
		(type INSTANCE)
;+		(allowed-classes Propiedad)
		(create-accessor read-write))
	(multislot Plato_noCompatibilidad
		(type INSTANCE)
;+		(allowed-classes Plato)
		(create-accessor read-write))
	(single-slot TipoComida_nombre
		(type STRING)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot Plato_nombre
		(type STRING)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(multislot Plato_ingredientes
		(type INSTANCE)
;+		(allowed-classes Ingrediente)
		(cardinality 1 ?VARIABLE)
		(create-accessor read-write))
	(single-slot Plato_bebida
		(type INSTANCE)
;+		(allowed-classes Bebida)
;+		(cardinality 0 1)
		(create-accessor read-write))
	(single-slot Lugar_nombre
		(type STRING)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot Bebida_precio
		(type INTEGER)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot Menu_bebida
		(type INSTANCE)
;+		(allowed-classes Bebida)
;+		(cardinality 0 1)
		(create-accessor read-write))
	(single-slot Plato_origen
		(type INSTANCE)
;+		(allowed-classes Lugar)
;+		(cardinality 0 1)
		(create-accessor read-write))
	(single-slot Plato_orden
		(type INTEGER)
		(range 1 3)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot Menu_postre
		(type INSTANCE)
;+		(allowed-classes Plato)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot EstiloComida_nombre
		(type STRING)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot Evento_epoca
		(type SYMBOL)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot Menu_primero
		(type INSTANCE)
;+		(allowed-classes Plato)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot Evento_tipo
		(type SYMBOL)
		(allowed-values FAMILIAR CONGRESO)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot Ingrediente_nombre
		(type STRING)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot Menu_segundo
		(type INSTANCE)
;+		(allowed-classes Plato)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(multislot Plato_tipos
		(type SYMBOL)
		(allowed-values FAMILIAR CONGRESO)
		(cardinality 1 ?VARIABLE)
		(create-accessor read-write))
	(multislot Bebida_noCompatibilidad
		(type INSTANCE)
;+		(allowed-classes Plato)
		(create-accessor read-write))
	(single-slot Plato_precio
		(type INTEGER)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(multislot Plato_tiposComida
		(type INSTANCE)
;+		(allowed-classes TipoComida)
		(create-accessor read-write))
	(single-slot Menu_precio
		(type INTEGER)
;+		(cardinality 1 1)
		(create-accessor read-write)))

(defclass Ingrediente
	(is-a USER)
	(role concrete)
	(single-slot Ingrediente_nombre
		(type STRING)
;+		(cardinality 1 1)
		(create-accessor read-write)))

(defclass Bebida
	(is-a USER)
	(role concrete)
	(single-slot Bebida_precio
		(type INTEGER)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot Bebida_alcholica
		(type SYMBOL)
		(allowed-values FALSE TRUE)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(multislot Bebida_noCompatibilidad
		(type INSTANCE)
;+		(allowed-classes Plato)
		(create-accessor read-write))
	(single-slot Bebida_nombre
		(type STRING)
;+		(cardinality 1 1)
		(create-accessor read-write)))

(defclass Plato
	(is-a USER)
	(role concrete)
	(single-slot Plato_nombre
		(type STRING)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(multislot Plato_ingredientes
		(type INSTANCE)
;+		(allowed-classes Ingrediente)
		(cardinality 1 ?VARIABLE)
		(create-accessor read-write))
	(single-slot Plato_orden
		(type INTEGER)
		(range 1 3)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(multislot Plato_tipos
		(type SYMBOL)
		(allowed-values FAMILIAR CONGRESO)
		(cardinality 1 ?VARIABLE)
		(create-accessor read-write))
	(multislot Plato_epocas
		(type SYMBOL)
		(allowed-values PRIMAVERA VERANO OTONO INVIERNO)
		(cardinality 1 ?VARIABLE)
		(create-accessor read-write))
	(single-slot Plato_precio
		(type INTEGER)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot Plato_complejidad
		(type INTEGER)
		(range 1 3)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot Plato_estiloComida
		(type INSTANCE)
;+		(allowed-classes EstiloComida)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(multislot Plato_tiposComida
		(type INSTANCE)
;+		(allowed-classes TipoComida)
		(create-accessor read-write))
	(multislot Plato_propiedades
		(type INSTANCE)
;+		(allowed-classes Propiedad)
		(create-accessor read-write))
	(multislot Plato_noCompatibilidad
		(type INSTANCE)
;+		(allowed-classes Plato)
		(create-accessor read-write))
	(single-slot Plato_origen
		(type INSTANCE)
;+		(allowed-classes Lugar)
;+		(cardinality 0 1)
		(create-accessor read-write)))

(defclass Lugar
	(is-a USER)
	(role concrete)
	(single-slot Lugar_nombre
		(type STRING)
;+		(cardinality 1 1)
		(create-accessor read-write)))

(defclass EstiloComida
	(is-a USER)
	(role concrete)
	(single-slot EstiloComida_nombre
		(type STRING)
;+		(cardinality 1 1)
		(create-accessor read-write)))

(defclass TipoComida
	(is-a USER)
	(role concrete)
	(single-slot TipoComida_nombre
		(type STRING)
;+		(cardinality 1 1)
		(create-accessor read-write)))

(defclass Menu
	(is-a USER)
	(role concrete)
	(single-slot Menu_primero
		(type INSTANCE)
;+		(allowed-classes Plato)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot Menu_postre
		(type INSTANCE)
;+		(allowed-classes Plato)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot Menu_segundo
		(type INSTANCE)
;+		(allowed-classes Plato)
;+		(cardinality 1 1)
		(create-accessor read-write))
	(single-slot Menu_bebida
		(type INSTANCE)
;+		(allowed-classes Bebida)
;+		(cardinality 0 1)
		(create-accessor read-write))
	(single-slot Menu_precio
		(type INTEGER)
;+		(cardinality 1 1)
		(create-accessor read-write)))

(defclass Propiedad
	(is-a USER)
	(role concrete)
	(single-slot Propiedad_nombre
		(type STRING)
;+		(cardinality 1 1)
		(create-accessor read-write)))
        
        
(definstances instancies

; Sat May 22 10:43:41 CEST 2021
; 
;+ (version "3.5")
;+ (build "Build 663")

([RicoRico2_Class10025] of  Plato

	(Plato_complejidad 3)
	(Plato_epocas PRIMAVERA VERANO OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class8])
	(Plato_ingredientes [RicoRico_Class31])
	(Plato_nombre "Coulant de chocolate")
	(Plato_orden 3)
	(Plato_precio 1200)
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida
		[RicoRico_Class38]
		[RicoRico_Class41]))

([RicoRico2_Class10026] of  Plato

	(Plato_complejidad 1)
	(Plato_epocas PRIMAVERA VERANO)
	(Plato_estiloComida [RicoRico_Class7])
	(Plato_ingredientes
		[RicoRico2_Class10027]
		[RicoRico2_Class10028]
		[RicoRico2_Class10029]
		[RicoRico2_Class10030])
	(Plato_nombre "Macedonia")
	(Plato_orden 3)
	(Plato_precio 1000)
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida
		[RicoRico_Class40]
		[RicoRico_Class39]
		[RicoRico_Class38]
		[RicoRico_Class41]))

([RicoRico2_Class10027] of  Ingrediente

	(Ingrediente_nombre "Fresas"))

([RicoRico2_Class10028] of  Ingrediente

	(Ingrediente_nombre "Platano"))

([RicoRico2_Class10029] of  Ingrediente

	(Ingrediente_nombre "Manzana"))

([RicoRico2_Class10030] of  Ingrediente

	(Ingrediente_nombre "Pera"))

([RicoRico2_Class10032] of  Plato

	(Plato_complejidad 3)
	(Plato_epocas PRIMAVERA VERANO OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class10])
	(Plato_ingredientes
		[RicoRico2_Class10033]
		[RicoRico2_Class10034]
		[RicoRico2_Class10035]
		[RicoRico2_Class10036]
		[RicoRico_Class34])
	(Plato_nombre "Carpaccio de ternera vegetariana")
	(Plato_orden 2)
	(Plato_precio 2000)
	(Plato_tipos FAMILIAR CONGRESO))

([RicoRico2_Class10033] of  Ingrediente

	(Ingrediente_nombre "Sandia"))

([RicoRico2_Class10034] of  Ingrediente

	(Ingrediente_nombre "Caldo vegetal"))

([RicoRico2_Class10035] of  Ingrediente

	(Ingrediente_nombre "Pimienta negra"))

([RicoRico2_Class10036] of  Ingrediente

	(Ingrediente_nombre "Tortitas crujientes"))

([RicoRico2_Class10037] of  Plato

	(Plato_complejidad 3)
	(Plato_epocas PRIMAVERA VERANO OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class10])
	(Plato_ingredientes
		[RicoRico_Class10015]
		[RicoRico2_Class10039]
		[RicoRico2_Class10040]
		[RicoRico2_Class10041]
		[RicoRico2_Class10042]
		[RicoRico2_Class10043]
		[RicoRico2_Class10044])
	(Plato_noCompatibilidad
		[RicoRico_Class51]
		[RicoRico_Class51]
		[RicoRico2_Class10079])
	(Plato_nombre "Paella de aroz negro con marisco fresco")
	(Plato_orden 1)
	(Plato_precio 2500)
	(Plato_propiedades [RicoRico_Class20074])
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida [RicoRico_Class41]))

([RicoRico2_Class10039] of  Ingrediente

	(Ingrediente_nombre "Arroz bomba"))

([RicoRico2_Class10040] of  Ingrediente

	(Ingrediente_nombre "Dientes de ajo"))

([RicoRico2_Class10041] of  Ingrediente

	(Ingrediente_nombre "Sepia"))

([RicoRico2_Class10042] of  Ingrediente

	(Ingrediente_nombre "Laurel"))

([RicoRico2_Class10043] of  Ingrediente

	(Ingrediente_nombre "Pimenton dulce"))

([RicoRico2_Class10044] of  Ingrediente

	(Ingrediente_nombre "Tinta de sepia"))

([RicoRico2_Class10045] of  Plato

	(Plato_complejidad 2)
	(Plato_epocas PRIMAVERA VERANO OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class10])
	(Plato_ingredientes
		[RicoRico_Class18]
		[RicoRico2_Class10047]
		[RicoRico2_Class10048]
		[RicoRico_Class21]
		[RicoRico_Class10024]
		[RicoRico2_Class10049]
		[RicoRico2_Class10050])
	(Plato_nombre "Surtido de canapes")
	(Plato_orden 3)
	(Plato_precio 1500)
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida [RicoRico_Class41]))

([RicoRico2_Class10047] of  Ingrediente

	(Ingrediente_nombre "Pan de molde"))

([RicoRico2_Class10048] of  Ingrediente

	(Ingrediente_nombre "Salmon ahumado"))

([RicoRico2_Class10049] of  Ingrediente

	(Ingrediente_nombre "Pepinillos"))

([RicoRico2_Class10050] of  Ingrediente

	(Ingrediente_nombre "Anchoas"))

([RicoRico2_Class10052] of  Plato

	(Plato_complejidad 1)
	(Plato_epocas PRIMAVERA VERANO OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class9])
	(Plato_ingredientes
		[RicoRico2_Class10054]
		[RicoRico2_Class10055]
		[RicoRico_Class22])
	(Plato_noCompatibilidad
		[RicoRico_Class53]
		[RicoRico_Class51])
	(Plato_nombre "Nigiri de aguacate")
	(Plato_orden 2)
	(Plato_origen [RicoRico2_Class10053])
	(Plato_precio 1000)
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida
		[RicoRico_Class40]
		[RicoRico_Class41]
		[RicoRico_Class39]
		[RicoRico_Class38]))

([RicoRico2_Class10053] of  Lugar

	(Lugar_nombre "Japon"))

([RicoRico2_Class10054] of  Ingrediente

	(Ingrediente_nombre "Alga nori"))

([RicoRico2_Class10055] of  Ingrediente

	(Ingrediente_nombre "Aguacate"))

([RicoRico2_Class10056] of  Plato

	(Plato_complejidad 1)
	(Plato_epocas PRIMAVERA VERANO OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class9])
	(Plato_ingredientes
		[RicoRico2_Class10057]
		[RicoRico2_Class10058]
		[RicoRico2_Class10059]
		[RicoRico_Class10015]
		[RicoRico_Class10016]
		[RicoRico_Class16])
	(Plato_nombre "Yakisoba de verduras")
	(Plato_orden 1)
	(Plato_origen [RicoRico2_Class10053])
	(Plato_precio 1200)
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida
		[RicoRico_Class41]
		[RicoRico_Class39]
		[RicoRico_Class38]))

([RicoRico2_Class10057] of  Ingrediente

	(Ingrediente_nombre "Fideos"))

([RicoRico2_Class10058] of  Ingrediente

	(Ingrediente_nombre "Salsa de soja"))

([RicoRico2_Class10059] of  Ingrediente

	(Ingrediente_nombre "Tofu"))

([RicoRico2_Class10060] of  Plato

	(Plato_complejidad 1)
	(Plato_epocas PRIMAVERA VERANO OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class9])
	(Plato_ingredientes
		[RicoRico2_Class10061]
		[RicoRico_Class25])
	(Plato_nombre "Mochis")
	(Plato_orden 3)
	(Plato_origen [RicoRico2_Class10053])
	(Plato_precio 1000)
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida
		[RicoRico_Class41]
		[RicoRico_Class39]
		[RicoRico_Class38]))

([RicoRico2_Class10061] of  Ingrediente

	(Ingrediente_nombre "Arroz glutinoso"))

([RicoRico2_Class10063] of  Plato

	(Plato_complejidad 1)
	(Plato_epocas PRIMAVERA)
	(Plato_estiloComida [RicoRico_Class8])
	(Plato_ingredientes
		[RicoRico2_Class10064]
		[RicoRico2_Class10065])
	(Plato_nombre "Torrijas de dulce de leche")
	(Plato_orden 3)
	(Plato_precio 1000)
	(Plato_tipos FAMILIAR)
	(Plato_tiposComida
		[RicoRico_Class41]
		[RicoRico_Class38]))

([RicoRico2_Class10064] of  Ingrediente

	(Ingrediente_nombre "Torrijas"))

([RicoRico2_Class10065] of  Ingrediente

	(Ingrediente_nombre "Dulce de leche"))

([RicoRico2_Class10067] of  Plato

	(Plato_complejidad 1)
	(Plato_epocas OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class7])
	(Plato_ingredientes
		[RicoRico2_Class10071]
		[RicoRico_Class23])
	(Plato_nombre "Lentejas a la marinera")
	(Plato_orden 1)
	(Plato_precio 1500)
	(Plato_propiedades [RicoRico_Class42])
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida [RicoRico_Class40]))

([RicoRico2_Class10071] of  Ingrediente

	(Ingrediente_nombre "Chorizo"))

([RicoRico2_Class10073] of  Plato

	(Plato_complejidad 3)
	(Plato_epocas VERANO PRIMAVERA)
	(Plato_estiloComida [RicoRico_Class7])
	(Plato_ingredientes
		[RicoRico2_Class10074]
		[RicoRico2_Class10075]
		[RicoRico_Class10015]
		[RicoRico_Class34]
		[RicoRico_Class29]
		[RicoRico_Class16]
		[RicoRico2_Class10042])
	(Plato_nombre "Osobuco a la milanesa")
	(Plato_orden 2)
	(Plato_precio 2500)
	(Plato_propiedades [RicoRico_Class20077])
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida [RicoRico_Class40]))

([RicoRico2_Class10074] of  Ingrediente

	(Ingrediente_nombre "Osobuco"))

([RicoRico2_Class10075] of  Ingrediente

	(Ingrediente_nombre "Apio"))

([RicoRico2_Class10076] of  Plato

	(Plato_complejidad 3)
	(Plato_epocas PRIMAVERA VERANO OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class10])
	(Plato_ingredientes
		[RicoRico_Class28]
		[RicoRico_Class21]
		[RicoRico_Class29]
		[RicoRico_Class25]
		[RicoRico2_Class10077]
		[RicoRico2_Class10078])
	(Plato_nombre "Croquembouche")
	(Plato_orden 3)
	(Plato_precio 2000)
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida
		[RicoRico_Class41]
		[RicoRico_Class38]))

([RicoRico2_Class10077] of  Ingrediente

	(Ingrediente_nombre "Miel"))

([RicoRico2_Class10078] of  Ingrediente

	(Ingrediente_nombre "Nata"))

([RicoRico2_Class10079] of  Plato

	(Plato_complejidad 3)
	(Plato_epocas PRIMAVERA VERANO OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class10])
	(Plato_ingredientes
		[RicoRico_Class10016]
		[RicoRico_Class26]
		[RicoRico2_Class10080]
		[RicoRico2_Class10081]
		[RicoRico2_Class10082]
		[RicoRico2_Class10083]
		[RicoRico2_Class10084]
		[RicoRico2_Class10085]
		[RicoRico_Class22])
	(Plato_noCompatibilidad
		[RicoRico_Class53]
		[RicoRico_Class51])
	(Plato_nombre "Paella de la huerta")
	(Plato_orden 1)
	(Plato_precio 3500)
	(Plato_propiedades
		[RicoRico_Class42]
		[RicoRico_Class20074])
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida
		[RicoRico_Class40]
		[RicoRico_Class41]
		[RicoRico_Class39]
		[RicoRico_Class38]))

([RicoRico2_Class10080] of  Ingrediente

	(Ingrediente_nombre "Guisantes"))

([RicoRico2_Class10081] of  Ingrediente

	(Ingrediente_nombre "Champinon"))

([RicoRico2_Class10082] of  Ingrediente

	(Ingrediente_nombre "Calabacin"))

([RicoRico2_Class10083] of  Ingrediente

	(Ingrediente_nombre "Azafran"))

([RicoRico2_Class10084] of  Ingrediente

	(Ingrediente_nombre "Aceite de oliva"))

([RicoRico2_Class10085] of  Ingrediente

	(Ingrediente_nombre "Curcuma"))

([RicoRico2_Class10086] of  Plato

	(Plato_complejidad 3)
	(Plato_epocas PRIMAVERA VERANO OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class10])
	(Plato_ingredientes
		[RicoRico_Class29]
		[RicoRico_Class28]
		[RicoRico_Class21]
		[RicoRico_Class34]
		[RicoRico2_Class10048]
		[RicoRico_Class20])
	(Plato_nombre "Profiteroles con salmon ahumado")
	(Plato_orden 2)
	(Plato_precio 3500)
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida
		[RicoRico_Class41]
		[RicoRico_Class38]))

([RicoRico2_Class10087] of  Plato

	(Plato_complejidad 2)
	(Plato_epocas PRIMAVERA INVIERNO OTONO VERANO)
	(Plato_estiloComida [RicoRico_Class8])
	(Plato_ingredientes
		[RicoRico_Class24]
		[RicoRico2_Class10078]
		[RicoRico2_Class10088]
		[RicoRico2_Class10089]
		[RicoRico2_Class10090])
	(Plato_nombre "Mousse de mazapam")
	(Plato_orden 3)
	(Plato_precio 1500)
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida
		[RicoRico_Class41]
		[RicoRico_Class38]))

([RicoRico2_Class10088] of  Ingrediente

	(Ingrediente_nombre "Mazapan"))

([RicoRico2_Class10089] of  Ingrediente

	(Ingrediente_nombre "Uvas"))

([RicoRico2_Class10090] of  Ingrediente

	(Ingrediente_nombre "Almendra picada"))

([RicoRico2_Class10091] of  Bebida

	(Bebida_alcholica FALSE)
	(Bebida_noCompatibilidad
		[RicoRico_Class20012]
		[RicoRico_Class10010]
		[RicoRico2_Class10037]
		[RicoRico2_Class10079]
		[RicoRico_Class20003]
		[RicoRico_Class20018])
	(Bebida_nombre "Fanta")
	(Bebida_precio 200))

([RicoRico2_Class10093] of  Bebida

	(Bebida_alcholica FALSE)
	(Bebida_nombre "Agua con gas")
	(Bebida_precio 175))

([RicoRico2_Class10094] of  Bebida

	(Bebida_alcholica TRUE)
	(Bebida_noCompatibilidad
		[RicoRico_Class53]
		[RicoRico_Class51]
		[RicoRico_Class46]
		[RicoRico_Class20019]
		[RicoRico_Class47]
		[RicoRico_Class20016])
	(Bebida_nombre "Vino")
	(Bebida_precio 700))

([RicoRico2_Class10095] of  Bebida

	(Bebida_alcholica TRUE)
	(Bebida_noCompatibilidad
		[RicoRico_Class53]
		[RicoRico_Class51]
		[RicoRico_Class20006]
		[RicoRico_Class56]
		[RicoRico_Class46]
		[RicoRico_Class47]
		[RicoRico_Class20016]
		[RicoRico_Class10017])
	(Bebida_nombre "Champan")
	(Bebida_precio 1200))

([RicoRico2_Class10098] of  Bebida

	(Bebida_alcholica FALSE)
	(Bebida_noCompatibilidad
		[RicoRico2_Class10037]
		[RicoRico2_Class10079]
		[RicoRico_Class20003]
		[RicoRico_Class20000]
		[RicoRico_Class20021]
		[RicoRico2_Class10045]
		[RicoRico_Class20007]
		[RicoRico2_Class10056])
	(Bebida_nombre "Zumo")
	(Bebida_precio 400))

([RicoRico_Class0] of  Bebida

	(Bebida_alcholica TRUE)
	(Bebida_noCompatibilidad
		[RicoRico_Class53]
		[RicoRico_Class56]
		[RicoRico_Class46]
		[RicoRico_Class20019]
		[RicoRico_Class47]
		[RicoRico_Class20016]
		[RicoRico_Class10017])
	(Bebida_nombre "Cava")
	(Bebida_precio 1000))

([RicoRico_Class10] of  EstiloComida

	(EstiloComida_nombre "Sibarita"))

([RicoRico_Class10000] of  Ingrediente

	(Ingrediente_nombre "Pollo"))

([RicoRico_Class10001] of  Ingrediente

	(Ingrediente_nombre "Kielbasa"))

([RicoRico_Class10002] of  Ingrediente

	(Ingrediente_nombre "Tallarines"))

([RicoRico_Class10003] of  Ingrediente

	(Ingrediente_nombre "Salchicha"))

([RicoRico_Class10004] of  Ingrediente

	(Ingrediente_nombre "Harina de maiz"))

([RicoRico_Class10005] of  Ingrediente

	(Ingrediente_nombre "Patata"))

([RicoRico_Class10008] of  Ingrediente

	(Ingrediente_nombre "Salsa marinada"))

([RicoRico_Class10009] of  Ingrediente

	(Ingrediente_nombre "Cerdo"))

([RicoRico_Class10010] of  Plato

	(Plato_complejidad 2)
	(Plato_epocas VERANO PRIMAVERA)
	(Plato_estiloComida [RicoRico_Class9])
	(Plato_ingredientes
		[RicoRico_Class10013]
		[RicoRico_Class10000])
	(Plato_nombre "Charqui jamaicano")
	(Plato_orden 2)
	(Plato_origen [RicoRico_Class10012])
	(Plato_precio 1000)
	(Plato_propiedades
		[RicoRico_Class42]
		[RicoRico_Class20077])
	(Plato_tipos FAMILIAR)
	(Plato_tiposComida [RicoRico_Class40]))

([RicoRico_Class10012] of  Lugar

	(Lugar_nombre "Jamaica"))

([RicoRico_Class10013] of  Ingrediente

	(Ingrediente_nombre "Limon"))

([RicoRico_Class10014] of  Plato

	(Plato_complejidad 2)
	(Plato_epocas VERANO PRIMAVERA OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class8])
	(Plato_ingredientes
		[RicoRico_Class10015]
		[RicoRico_Class33]
		[RicoRico_Class10016])
	(Plato_nombre "Lasana")
	(Plato_orden 2)
	(Plato_precio 800)
	(Plato_propiedades
		[RicoRico_Class42]
		[RicoRico_Class20077])
	(Plato_tipos FAMILIAR CONGRESO))

([RicoRico_Class10015] of  Ingrediente

	(Ingrediente_nombre "Cebolla"))

([RicoRico_Class10016] of  Ingrediente

	(Ingrediente_nombre "Pimiento"))

([RicoRico_Class10017] of  Plato

	(Plato_complejidad 1)
	(Plato_epocas PRIMAVERA VERANO OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class9])
	(Plato_ingredientes
		[RicoRico_Class13]
		[RicoRico_Class10019])
	(Plato_noCompatibilidad
		[RicoRico_Class46]
		[RicoRico_Class47])
	(Plato_nombre "Espaguetis sin gluten")
	(Plato_orden 1)
	(Plato_origen [RicoRico_Class35])
	(Plato_precio 700)
	(Plato_propiedades
		[RicoRico_Class42]
		[RicoRico_Class20072])
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida
		[RicoRico_Class40]
		[RicoRico_Class41]
		[RicoRico_Class39]
		[RicoRico_Class38]))

([RicoRico_Class10019] of  Ingrediente

	(Ingrediente_nombre "Salsa de tomate"))

([RicoRico_Class10020] of  Plato

	(Plato_complejidad 3)
	(Plato_epocas VERANO)
	(Plato_estiloComida [RicoRico_Class9])
	(Plato_ingredientes
		[RicoRico_Class10022]
		[RicoRico_Class10023]
		[RicoRico_Class26]
		[RicoRico_Class18]
		[RicoRico_Class20]
		[RicoRico_Class10024]
		[RicoRico_Class34])
	(Plato_noCompatibilidad
		[RicoRico_Class47]
		[RicoRico_Class46]
		[RicoRico_Class20001])
	(Plato_nombre "Ensalada de cus cus veraniega")
	(Plato_orden 1)
	(Plato_origen [RicoRico_Class10021])
	(Plato_precio 700)
	(Plato_propiedades [RicoRico_Class20073])
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida
		[RicoRico_Class40]
		[RicoRico_Class41]
		[RicoRico_Class39]
		[RicoRico_Class38]))

([RicoRico_Class10021] of  Lugar

	(Lugar_nombre "Marruecos"))

([RicoRico_Class10022] of  Ingrediente

	(Ingrediente_nombre "Cus cus"))

([RicoRico_Class10023] of  Ingrediente

	(Ingrediente_nombre "Caldo de pollo"))

([RicoRico_Class10024] of  Ingrediente

	(Ingrediente_nombre "Vinagre"))

([RicoRico_Class11] of  Bebida

	(Bebida_alcholica FALSE)
	(Bebida_nombre "Agua")
	(Bebida_precio 150))

([RicoRico_Class12] of  Bebida

	(Bebida_alcholica FALSE)
	(Bebida_noCompatibilidad
		[RicoRico_Class20001]
		[RicoRico2_Class10067]
		[RicoRico2_Class10052]
		[RicoRico_Class20003]
		[RicoRico_Class20000])
	(Bebida_nombre "CocaCola")
	(Bebida_precio 250))

([RicoRico_Class13] of  Ingrediente

	(Ingrediente_nombre "Pasta"))

([RicoRico_Class14] of  Ingrediente

	(Ingrediente_nombre "Lechuga"))

([RicoRico_Class15] of  Ingrediente

	(Ingrediente_nombre "Pepino"))

([RicoRico_Class16] of  Ingrediente

	(Ingrediente_nombre "Zanahoria"))

([RicoRico_Class17] of  Ingrediente

	(Ingrediente_nombre "Huevo duro"))

([RicoRico_Class18] of  Ingrediente

	(Ingrediente_nombre "Olivas"))

([RicoRico_Class19] of  Ingrediente

	(Ingrediente_nombre "Macarrones"))

([RicoRico_Class20] of  Ingrediente

	(Ingrediente_nombre "Queso"))

([RicoRico_Class20000] of  Plato

	(Plato_complejidad 2)
	(Plato_epocas PRIMAVERA VERANO OTONO)
	(Plato_estiloComida [RicoRico_Class7])
	(Plato_ingredientes
		[RicoRico_Class20032]
		[RicoRico2_Class10081]
		[RicoRico_Class20033]
		[RicoRico_Class20034]
		[RicoRico_Class20035])
	(Plato_nombre "Pescado blanco a la plancha con tortellini")
	(Plato_orden 1)
	(Plato_precio 2000)
	(Plato_propiedades [RicoRico_Class20074])
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida [RicoRico_Class41]))

([RicoRico_Class20001] of  Plato

	(Plato_complejidad 2)
	(Plato_epocas INVIERNO OTONO VERANO PRIMAVERA)
	(Plato_estiloComida [RicoRico_Class7])
	(Plato_ingredientes
		[RicoRico_Class25]
		[RicoRico_Class20036]
		[RicoRico_Class16]
		[RicoRico_Class26]
		[RicoRico_Class20037])
	(Plato_noCompatibilidad
		[RicoRico_Class10020]
		[RicoRico_Class46])
	(Plato_nombre "Ensalada con jarabe de rosa mosqueta")
	(Plato_orden 2)
	(Plato_precio 1300)
	(Plato_propiedades [RicoRico_Class20073])
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida
		[RicoRico_Class40]
		[RicoRico_Class41]
		[RicoRico_Class39]
		[RicoRico_Class38]))

([RicoRico_Class20002] of  Plato

	(Plato_complejidad 1)
	(Plato_epocas PRIMAVERA VERANO INVIERNO OTONO)
	(Plato_estiloComida [RicoRico_Class7])
	(Plato_ingredientes
		[RicoRico_Class24]
		[RicoRico2_Class10027]
		[RicoRico_Class25])
	(Plato_nombre "Yogurt de fresa")
	(Plato_orden 3)
	(Plato_precio 700)
	(Plato_tipos FAMILIAR)
	(Plato_tiposComida
		[RicoRico_Class40]
		[RicoRico_Class41]
		[RicoRico_Class38]))

([RicoRico_Class20003] of  Plato

	(Plato_complejidad 2)
	(Plato_epocas PRIMAVERA VERANO OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class8])
	(Plato_ingredientes
		[RicoRico_Class20039]
		[RicoRico_Class20040]
		[RicoRico_Class20041])
	(Plato_nombre "Pasta con langostinos y salsa de tomate")
	(Plato_orden 1)
	(Plato_precio 1500)
	(Plato_propiedades [RicoRico_Class20072])
	(Plato_tipos CONGRESO)
	(Plato_tiposComida [RicoRico_Class41]))

([RicoRico_Class20004] of  Plato

	(Plato_complejidad 2)
	(Plato_epocas PRIMAVERA VERANO OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class8])
	(Plato_ingredientes
		[RicoRico_Class28]
		[RicoRico_Class20]
		[RicoRico_Class20038]
		[RicoRico2_Class10084]
		[RicoRico_Class24])
	(Plato_nombre "Bollos de romero y oliva con queso de cabra")
	(Plato_orden 2)
	(Plato_precio 900)
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida
		[RicoRico_Class40]
		[RicoRico_Class41]
		[RicoRico_Class38]))

([RicoRico_Class20005] of  Plato

	(Plato_complejidad 1)
	(Plato_epocas OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class8])
	(Plato_ingredientes
		[RicoRico_Class20]
		[RicoRico_Class25]
		[RicoRico_Class21]
		[RicoRico_Class20042]
		[RicoRico_Class20043])
	(Plato_nombre "Tiramisu")
	(Plato_orden 3)
	(Plato_precio 900)
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida
		[RicoRico_Class41]
		[RicoRico_Class38]))

([RicoRico_Class20006] of  Plato

	(Plato_complejidad 1)
	(Plato_epocas PRIMAVERA OTONO VERANO INVIERNO)
	(Plato_estiloComida [RicoRico_Class9])
	(Plato_ingredientes
		[RicoRico_Class22]
		[RicoRico_Class20028]
		[RicoRico_Class17]
		[RicoRico_Class20029])
	(Plato_nombre "Burritos de salmon con huevo")
	(Plato_orden 1)
	(Plato_origen [RicoRico_Class20027])
	(Plato_precio 1500)
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida
		[RicoRico_Class40]
		[RicoRico_Class41]))

([RicoRico_Class20007] of  Plato

	(Plato_complejidad 2)
	(Plato_epocas PRIMAVERA VERANO)
	(Plato_estiloComida [RicoRico_Class9])
	(Plato_ingredientes
		[RicoRico2_Class10084]
		[RicoRico_Class20025]
		[RicoRico_Class10013]
		[RicoRico2_Class10077]
		[RicoRico_Class20026]
		[RicoRico_Class10015]
		[RicoRico_Class26])
	(Plato_nombre "Tayin de cordero")
	(Plato_orden 2)
	(Plato_origen [RicoRico_Class20024])
	(Plato_precio 1000)
	(Plato_propiedades
		[RicoRico_Class42]
		[RicoRico_Class20077])
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida [RicoRico_Class40]))

([RicoRico_Class20008] of  Plato

	(Plato_complejidad 1)
	(Plato_epocas OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class9])
	(Plato_ingredientes
		[RicoRico_Class21]
		[RicoRico_Class20031]
		[RicoRico_Class25])
	(Plato_nombre "Macarones")
	(Plato_orden 3)
	(Plato_origen [RicoRico_Class20030])
	(Plato_precio 1200)
	(Plato_propiedades [RicoRico_Class20072])
	(Plato_tipos FAMILIAR)
	(Plato_tiposComida
		[RicoRico_Class41]
		[RicoRico_Class38]))

([RicoRico_Class20009] of  Plato

	(Plato_complejidad 3)
	(Plato_epocas PRIMAVERA VERANO OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class10])
	(Plato_ingredientes
		[RicoRico_Class20044]
		[RicoRico2_Class10084]
		[RicoRico_Class20034]
		[RicoRico_Class20045]
		[RicoRico_Class10015]
		[RicoRico2_Class10035]
		[RicoRico_Class34]
		[RicoRico_Class20063])
	(Plato_nombre "Gratinado de nueces y maiz dulce")
	(Plato_orden 1)
	(Plato_precio 2500)
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida
		[RicoRico_Class40]
		[RicoRico_Class41]
		[RicoRico_Class38]))

([RicoRico_Class20010] of  Plato

	(Plato_complejidad 3)
	(Plato_epocas PRIMAVERA VERANO OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class10])
	(Plato_ingredientes
		[RicoRico_Class20060]
		[RicoRico_Class20061]
		[RicoRico2_Class10084]
		[RicoRico_Class20062])
	(Plato_nombre "Filete con salsa cabernet de hongos")
	(Plato_orden 2)
	(Plato_precio 2000)
	(Plato_propiedades
		[RicoRico_Class42]
		[RicoRico_Class20077])
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida [RicoRico_Class40]))

([RicoRico_Class20011] of  Plato

	(Plato_complejidad 2)
	(Plato_epocas PRIMAVERA OTONO VERANO INVIERNO)
	(Plato_estiloComida [RicoRico_Class10])
	(Plato_ingredientes
		[RicoRico_Class24]
		[RicoRico_Class20059]
		[RicoRico_Class25])
	(Plato_nombre "Kulfi de canela")
	(Plato_orden 3)
	(Plato_precio 1000)
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida
		[RicoRico_Class40]
		[RicoRico_Class41]
		[RicoRico_Class38]))

([RicoRico_Class20012] of  Plato

	(Plato_complejidad 2)
	(Plato_epocas PRIMAVERA OTONO)
	(Plato_estiloComida [RicoRico_Class7])
	(Plato_ingredientes
		[RicoRico_Class20048]
		[RicoRico2_Class10084]
		[RicoRico2_Class10035]
		[RicoRico_Class20049])
	(Plato_nombre "Berenjena asada con limon")
	(Plato_orden 1)
	(Plato_precio 1350)
	(Plato_tipos FAMILIAR))

([RicoRico_Class20013] of  Plato

	(Plato_complejidad 2)
	(Plato_epocas PRIMAVERA VERANO OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class7])
	(Plato_ingredientes
		[RicoRico_Class20046]
		[RicoRico_Class25]
		[RicoRico_Class34]
		[RicoRico_Class20047])
	(Plato_nombre "Cerdo asado crujiente")
	(Plato_orden 2)
	(Plato_precio 1400)
	(Plato_propiedades
		[RicoRico_Class42]
		[RicoRico_Class20077])
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida [RicoRico_Class40]))

([RicoRico_Class20014] of  Plato

	(Plato_complejidad 2)
	(Plato_epocas OTONO)
	(Plato_estiloComida [RicoRico_Class7])
	(Plato_ingredientes
		[RicoRico_Class20044]
		[RicoRico_Class25]
		[RicoRico_Class21]
		[RicoRico_Class20045]
		[RicoRico_Class32]
		[RicoRico_Class34])
	(Plato_nombre "Pan de calabaza y queso crema")
	(Plato_orden 3)
	(Plato_precio 700)
	(Plato_tipos FAMILIAR))

([RicoRico_Class20015] of  Plato

	(Plato_complejidad 2)
	(Plato_epocas PRIMAVERA INVIERNO OTONO VERANO)
	(Plato_estiloComida [RicoRico_Class8])
	(Plato_ingredientes
		[RicoRico_Class10000]
		[RicoRico2_Class10084]
		[RicoRico_Class26]
		[RicoRico_Class20058]
		[RicoRico_Class18])
	(Plato_nombre "Pollo a la plancha con limon y tomillo")
	(Plato_orden 2)
	(Plato_precio 1450)
	(Plato_propiedades [RicoRico_Class42])
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida [RicoRico_Class40]))

([RicoRico_Class20016] of  Plato

	(Plato_complejidad 1)
	(Plato_epocas PRIMAVERA VERANO OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class8])
	(Plato_ingredientes
		[RicoRico_Class26]
		[RicoRico_Class20])
	(Plato_nombre "Pizza margarita")
	(Plato_orden 1)
	(Plato_precio 900)
	(Plato_tipos FAMILIAR)
	(Plato_tiposComida
		[RicoRico_Class41]
		[RicoRico_Class38]))

([RicoRico_Class20017] of  Plato

	(Plato_complejidad 2)
	(Plato_epocas PRIMAVERA OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class8])
	(Plato_ingredientes
		[RicoRico_Class20044]
		[RicoRico_Class28]
		[RicoRico_Class29]
		[RicoRico_Class20059])
	(Plato_nombre "Pastel de pan de calabaza")
	(Plato_orden 3)
	(Plato_precio 1200)
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida
		[RicoRico_Class40]
		[RicoRico_Class41]
		[RicoRico_Class39]
		[RicoRico_Class38]))

([RicoRico_Class20018] of  Plato

	(Plato_complejidad 3)
	(Plato_epocas PRIMAVERA VERANO OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class9])
	(Plato_ingredientes
		[RicoRico2_Class10057]
		[RicoRico2_Class10084]
		[RicoRico2_Class10081]
		[RicoRico_Class20049]
		[RicoRico_Class20052]
		[RicoRico_Class20053]
		[RicoRico_Class10023])
	(Plato_nombre "Sopa de curry tailandes")
	(Plato_orden 1)
	(Plato_origen [RicoRico_Class20050])
	(Plato_precio 900)
	(Plato_propiedades
		[RicoRico_Class42]
		[RicoRico_Class20075])
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida [RicoRico_Class41]))

([RicoRico_Class20019] of  Plato

	(Plato_complejidad 1)
	(Plato_epocas PRIMAVERA VERANO)
	(Plato_estiloComida [RicoRico_Class9])
	(Plato_ingredientes
		[RicoRico_Class10005]
		[RicoRico_Class20035]
		[RicoRico_Class34]
		[RicoRico2_Class10084])
	(Plato_nombre "Fish and chips")
	(Plato_orden 2)
	(Plato_origen [RicoRico_Class20054])
	(Plato_precio 1200)
	(Plato_propiedades
		[RicoRico_Class42]
		[RicoRico_Class20074])
	(Plato_tipos FAMILIAR)
	(Plato_tiposComida
		[RicoRico_Class40]
		[RicoRico_Class41]))

([RicoRico_Class20020] of  Plato

	(Plato_complejidad 1)
	(Plato_epocas PRIMAVERA OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class9])
	(Plato_ingredientes
		[RicoRico_Class28]
		[RicoRico_Class32]
		[RicoRico_Class21]
		[RicoRico_Class20056]
		[RicoRico_Class20057])
	(Plato_nombre "Panqueques de jengibre")
	(Plato_orden 3)
	(Plato_origen [RicoRico_Class20055])
	(Plato_precio 800)
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida
		[RicoRico_Class41]
		[RicoRico_Class39]))

([RicoRico_Class20021] of  Plato

	(Plato_complejidad 2)
	(Plato_epocas PRIMAVERA VERANO OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class10])
	(Plato_ingredientes
		[RicoRico_Class10015]
		[RicoRico_Class20066]
		[RicoRico2_Class10080]
		[RicoRico_Class22]
		[RicoRico_Class20067]
		[RicoRico_Class20068])
	(Plato_nombre "Risotto de guisantes y estragon")
	(Plato_orden 1)
	(Plato_precio 1750)
	(Plato_tipos CONGRESO FAMILIAR)
	(Plato_tiposComida
		[RicoRico_Class40]
		[RicoRico_Class41]
		[RicoRico_Class39]
		[RicoRico_Class38]))

([RicoRico_Class20022] of  Plato

	(Plato_complejidad 2)
	(Plato_epocas PRIMAVERA VERANO)
	(Plato_estiloComida [RicoRico_Class10])
	(Plato_ingredientes
		[RicoRico_Class10015]
		[RicoRico2_Class10084]
		[RicoRico_Class20064]
		[RicoRico_Class10013]
		[RicoRico_Class20065])
	(Plato_nombre "Vieiras de salmon")
	(Plato_orden 2)
	(Plato_precio 1800)
	(Plato_propiedades [RicoRico_Class20074])
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida
		[RicoRico_Class40]
		[RicoRico_Class41]))

([RicoRico_Class20023] of  Plato

	(Plato_complejidad 2)
	(Plato_epocas PRIMAVERA VERANO OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class10])
	(Plato_ingredientes
		[RicoRico_Class20069]
		[RicoRico_Class20070]
		[RicoRico_Class21]
		[RicoRico_Class20056]
		[RicoRico_Class25]
		[RicoRico_Class20071])
	(Plato_nombre "Mousse de castanas")
	(Plato_orden 3)
	(Plato_precio 900)
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida
		[RicoRico_Class41]
		[RicoRico_Class38]))

([RicoRico_Class20024] of  Lugar

	(Lugar_nombre "Sudafrica"))

([RicoRico_Class20025] of  Ingrediente

	(Ingrediente_nombre "Cordero"))

([RicoRico_Class20026] of  Ingrediente

	(Ingrediente_nombre "Comino"))

([RicoRico_Class20027] of  Lugar

	(Lugar_nombre "Mexico"))

([RicoRico_Class20028] of  Ingrediente

	(Ingrediente_nombre "Salmon"))

([RicoRico_Class20029] of  Ingrediente

	(Ingrediente_nombre "Mostaza"))

([RicoRico_Class20030] of  Lugar

	(Lugar_nombre "Francia"))

([RicoRico_Class20031] of  Ingrediente

	(Ingrediente_nombre "Almendra"))

([RicoRico_Class20032] of  Ingrediente

	(Ingrediente_nombre "Tortellini"))

([RicoRico_Class20033] of  Ingrediente

	(Ingrediente_nombre "Col"))

([RicoRico_Class20034] of  Ingrediente

	(Ingrediente_nombre "Parmesano"))

([RicoRico_Class20035] of  Ingrediente

	(Ingrediente_nombre "Pescado"))

([RicoRico_Class20036] of  Ingrediente

	(Ingrediente_nombre "Escaramujos"))

([RicoRico_Class20037] of  Ingrediente

	(Ingrediente_nombre "Rucula"))

([RicoRico_Class20038] of  Ingrediente

	(Ingrediente_nombre "Tomate cherry"))

([RicoRico_Class20039] of  Ingrediente

	(Ingrediente_nombre "Gambas"))

([RicoRico_Class20040] of  Ingrediente

	(Ingrediente_nombre "Brocoli"))

([RicoRico_Class20041] of  Ingrediente

	(Ingrediente_nombre "Espaguetis"))

([RicoRico_Class20042] of  Ingrediente

	(Ingrediente_nombre "Cafe"))

([RicoRico_Class20043] of  Ingrediente

	(Ingrediente_nombre "Cacao"))

([RicoRico_Class20044] of  Ingrediente

	(Ingrediente_nombre "Calabaza"))

([RicoRico_Class20045] of  Ingrediente

	(Ingrediente_nombre "Nueces"))

([RicoRico_Class20046] of  Ingrediente

	(Ingrediente_nombre "Panceta"))

([RicoRico_Class20047] of  Ingrediente

	(Ingrediente_nombre "Especias"))

([RicoRico_Class20048] of  Ingrediente

	(Ingrediente_nombre "Berenjena"))

([RicoRico_Class20049] of  Ingrediente

	(Ingrediente_nombre "Jugo de limon"))

([RicoRico_Class20050] of  Lugar

	(Lugar_nombre "Tailandia"))

([RicoRico_Class20052] of  Ingrediente

	(Ingrediente_nombre "Salsa roja"))

([RicoRico_Class20053] of  Ingrediente

	(Ingrediente_nombre "Cilantro"))

([RicoRico_Class20054] of  Lugar

	(Lugar_nombre "Inglaterra"))

([RicoRico_Class20055] of  Lugar

	(Lugar_nombre "Grecia"))

([RicoRico_Class20056] of  Ingrediente

	(Ingrediente_nombre "Vainilla"))

([RicoRico_Class20057] of  Ingrediente

	(Ingrediente_nombre "Jengibre"))

([RicoRico_Class20058] of  Ingrediente

	(Ingrediente_nombre "Humus"))

([RicoRico_Class20059] of  Ingrediente

	(Ingrediente_nombre "Canela"))

([RicoRico_Class20060] of  Ingrediente

	(Ingrediente_nombre "Filete de ternera"))

([RicoRico_Class20061] of  Ingrediente

	(Ingrediente_nombre "Vino"))

([RicoRico_Class20062] of  Ingrediente

	(Ingrediente_nombre "Esparragos verdes"))

([RicoRico_Class20063] of  Ingrediente

	(Ingrediente_nombre "Maiz dulce"))

([RicoRico_Class20064] of  Ingrediente

	(Ingrediente_nombre "Vieiras"))

([RicoRico_Class20065] of  Ingrediente

	(Ingrediente_nombre "Perejil"))

([RicoRico_Class20066] of  Ingrediente

	(Ingrediente_nombre "Caldo de verduras"))

([RicoRico_Class20067] of  Ingrediente

	(Ingrediente_nombre "Estragon picado"))

([RicoRico_Class20068] of  Ingrediente

	(Ingrediente_nombre "Ajo"))

([RicoRico_Class20069] of  Ingrediente

	(Ingrediente_nombre "Castanas"))

([RicoRico_Class20070] of  Ingrediente

	(Ingrediente_nombre "Naranja"))

([RicoRico_Class20071] of  Ingrediente

	(Ingrediente_nombre "Nata montada"))

([RicoRico_Class20072] of  Propiedad

	(Propiedad_nombre "Pasta"))

([RicoRico_Class20073] of  Propiedad

	(Propiedad_nombre "Ensalada"))

([RicoRico_Class20074] of  Propiedad

	(Propiedad_nombre "Pescado y marisco"))

([RicoRico_Class20075] of  Propiedad

	(Propiedad_nombre "Sopa"))

([RicoRico_Class20077] of  Propiedad

	(Propiedad_nombre "Carne"))

([RicoRico_Class21] of  Ingrediente

	(Ingrediente_nombre "Huevo"))

([RicoRico_Class22] of  Ingrediente

	(Ingrediente_nombre "Arroz"))

([RicoRico_Class23] of  Ingrediente

	(Ingrediente_nombre "Lentejas"))

([RicoRico_Class24] of  Ingrediente

	(Ingrediente_nombre "Leche"))

([RicoRico_Class25] of  Ingrediente

	(Ingrediente_nombre "Azucar"))

([RicoRico_Class26] of  Ingrediente

	(Ingrediente_nombre "Tomate"))

([RicoRico_Class27] of  Ingrediente

	(Ingrediente_nombre "Garbanzos"))

([RicoRico_Class28] of  Ingrediente

	(Ingrediente_nombre "Harina"))

([RicoRico_Class29] of  Ingrediente

	(Ingrediente_nombre "Mantequilla"))

([RicoRico_Class30] of  Ingrediente

	(Ingrediente_nombre "Jamon"))

([RicoRico_Class30001] of  Bebida

	(Bebida_alcholica TRUE)
	(Bebida_noCompatibilidad
		[RicoRico_Class53]
		[RicoRico_Class20012]
		[RicoRico2_Class10032]
		[RicoRico2_Class10025]
		[RicoRico_Class54]
		[RicoRico2_Class10076]
		[RicoRico_Class56]
		[RicoRico_Class46]
		[RicoRico_Class52]
		[RicoRico_Class10014]
		[RicoRico_Class20008]
		[RicoRico_Class47]
		[RicoRico2_Class10060]
		[RicoRico_Class20023]
		[RicoRico2_Class10087]
		[RicoRico2_Class10037]
		[RicoRico_Class20014]
		[RicoRico_Class20020]
		[RicoRico_Class20003]
		[RicoRico_Class57]
		[RicoRico_Class20000]
		[RicoRico_Class20016]
		[RicoRico2_Class10086]
		[RicoRico_Class20021]
		[RicoRico_Class20018]
		[RicoRico2_Class10045]
		[RicoRico_Class20005]
		[RicoRico2_Class10063]
		[RicoRico_Class20022]
		[RicoRico2_Class10056])
	(Bebida_nombre "Cerveza")
	(Bebida_precio 300))

([RicoRico_Class30004] of  Bebida

	(Bebida_alcholica FALSE)
	(Bebida_noCompatibilidad
		[RicoRico_Class53]
		[RicoRico2_Class10037]
		[RicoRico2_Class10079]
		[RicoRico_Class20003]
		[RicoRico_Class20000]
		[RicoRico_Class20021]
		[RicoRico_Class20018])
	(Bebida_nombre "Nestea")
	(Bebida_precio 250))

([RicoRico_Class30005] of  Bebida

	(Bebida_alcholica FALSE)
	(Bebida_noCompatibilidad
		[RicoRico_Class20012]
		[RicoRico_Class10010]
		[RicoRico_Class20010]
		[RicoRico2_Class10067]
		[RicoRico2_Class10037]
		[RicoRico_Class20003]
		[RicoRico_Class20015]
		[RicoRico_Class20021]
		[RicoRico_Class20007]
		[RicoRico2_Class10056])
	(Bebida_nombre "Trina")
	(Bebida_precio 250))

([RicoRico_Class30007] of  Bebida

	(Bebida_alcholica TRUE)
	(Bebida_noCompatibilidad
		[RicoRico_Class53]
		[RicoRico_Class20012]
		[RicoRico2_Class10032]
		[RicoRico2_Class10025]
		[RicoRico_Class54]
		[RicoRico2_Class10076]
		[RicoRico_Class56]
		[RicoRico_Class46]
		[RicoRico_Class52]
		[RicoRico_Class10014]
		[RicoRico_Class20008]
		[RicoRico_Class47]
		[RicoRico2_Class10060]
		[RicoRico_Class20023]
		[RicoRico2_Class10087]
		[RicoRico2_Class10037]
		[RicoRico_Class20014]
		[RicoRico_Class20020]
		[RicoRico_Class20003]
		[RicoRico_Class57]
		[RicoRico_Class20000]
		[RicoRico_Class20016]
		[RicoRico2_Class10086]
		[RicoRico_Class20021]
		[RicoRico_Class20018]
		[RicoRico2_Class10045]
		[RicoRico_Class20005]
		[RicoRico2_Class10063]
		[RicoRico_Class20022]
		[RicoRico2_Class10056])
	(Bebida_nombre "Cerveza artesana")
	(Bebida_precio 800))

([RicoRico_Class31] of  Ingrediente

	(Ingrediente_nombre "Chocolate"))

([RicoRico_Class32] of  Ingrediente

	(Ingrediente_nombre "Levadura"))

([RicoRico_Class33] of  Ingrediente

	(Ingrediente_nombre "Ternera"))

([RicoRico_Class34] of  Ingrediente

	(Ingrediente_nombre "Sal"))

([RicoRico_Class35] of  Lugar

	(Lugar_nombre "Italia"))

([RicoRico_Class36] of  Lugar

	(Lugar_nombre "Espana"))

([RicoRico_Class38] of  TipoComida

	(TipoComida_nombre "Vegetariana"))

([RicoRico_Class39] of  TipoComida

	(TipoComida_nombre "Vegana"))

([RicoRico_Class40] of  TipoComida

	(TipoComida_nombre "Apta para celiacos"))

([RicoRico_Class41] of  TipoComida

	(TipoComida_nombre "Apta para musulmanes"))

([RicoRico_Class42] of  Propiedad

	(Propiedad_nombre "Caliente"))

([RicoRico_Class46] of  Plato

	(Plato_complejidad 1)
	(Plato_epocas PRIMAVERA VERANO OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class8])
	(Plato_ingredientes
		[RicoRico_Class17]
		[RicoRico_Class14]
		[RicoRico_Class18]
		[RicoRico_Class13]
		[RicoRico_Class15]
		[RicoRico_Class16])
	(Plato_noCompatibilidad
		[RicoRico_Class47]
		[RicoRico_Class10017]
		[RicoRico_Class20001]
		[RicoRico_Class10020])
	(Plato_nombre "Ensalada de pasta")
	(Plato_orden 1)
	(Plato_precio 500)
	(Plato_propiedades
		[RicoRico_Class20073]
		[RicoRico_Class20072])
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida
		[RicoRico_Class41]
		[RicoRico_Class38]))

([RicoRico_Class47] of  Plato

	(Plato_complejidad 1)
	(Plato_epocas PRIMAVERA VERANO OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class8])
	(Plato_ingredientes
		[RicoRico_Class21]
		[RicoRico_Class19]
		[RicoRico_Class20]
		[RicoRico_Class50])
	(Plato_noCompatibilidad
		[RicoRico_Class46]
		[RicoRico_Class10017])
	(Plato_nombre "Macarrones a la carbonara")
	(Plato_orden 2)
	(Plato_precio 700)
	(Plato_propiedades [RicoRico_Class20072])
	(Plato_tipos FAMILIAR))

([RicoRico_Class50] of  Ingrediente

	(Ingrediente_nombre "Bacon"))

([RicoRico_Class51] of  Plato

	(Plato_complejidad 1)
	(Plato_epocas PRIMAVERA VERANO OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class8])
	(Plato_ingredientes
		[RicoRico_Class22]
		[RicoRico_Class23])
	(Plato_noCompatibilidad
		[RicoRico_Class53]
		[RicoRico2_Class10037]
		[RicoRico2_Class10037]
		[RicoRico2_Class10079])
	(Plato_nombre "Arroz con lentejas")
	(Plato_orden 1)
	(Plato_precio 800)
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida
		[RicoRico_Class40]
		[RicoRico_Class41]
		[RicoRico_Class39]
		[RicoRico_Class38]))

([RicoRico_Class52] of  Plato

	(Plato_complejidad 2)
	(Plato_epocas PRIMAVERA VERANO OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class7])
	(Plato_ingredientes
		[RicoRico_Class21]
		[RicoRico_Class24]
		[RicoRico_Class25])
	(Plato_nombre "Flan de huevo")
	(Plato_orden 3)
	(Plato_precio 600)
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida
		[RicoRico_Class41]
		[RicoRico_Class38]))

([RicoRico_Class53] of  Plato

	(Plato_complejidad 1)
	(Plato_epocas PRIMAVERA VERANO OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class7])
	(Plato_ingredientes
		[RicoRico_Class22]
		[RicoRico_Class21]
		[RicoRico_Class26])
	(Plato_noCompatibilidad
		[RicoRico_Class51]
		[RicoRico2_Class10037]
		[RicoRico2_Class10052]
		[RicoRico2_Class10052]
		[RicoRico2_Class10037]
		[RicoRico2_Class10079])
	(Plato_nombre "Arroz a la cubana")
	(Plato_orden 1)
	(Plato_precio 700)
	(Plato_propiedades [RicoRico_Class42])
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida [RicoRico_Class38]))

([RicoRico_Class54] of  Plato

	(Plato_complejidad 2)
	(Plato_epocas PRIMAVERA VERANO OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class9])
	(Plato_ingredientes
		[RicoRico_Class21]
		[RicoRico_Class24]
		[RicoRico_Class25])
	(Plato_nombre "Crema catalana")
	(Plato_orden 3)
	(Plato_origen [RicoRico_Class36])
	(Plato_precio 700)
	(Plato_tipos FAMILIAR)
	(Plato_tiposComida
		[RicoRico_Class41]
		[RicoRico_Class38]))

([RicoRico_Class56] of  Plato

	(Plato_complejidad 1)
	(Plato_epocas PRIMAVERA VERANO OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class9])
	(Plato_ingredientes
		[RicoRico_Class28]
		[RicoRico_Class21]
		[RicoRico_Class30])
	(Plato_nombre "Croquetas de jamon")
	(Plato_orden 2)
	(Plato_origen [RicoRico_Class36])
	(Plato_precio 600)
	(Plato_tipos FAMILIAR CONGRESO))

([RicoRico_Class57] of  Plato

	(Plato_complejidad 2)
	(Plato_epocas PRIMAVERA VERANO OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class7])
	(Plato_ingredientes
		[RicoRico_Class31]
		[RicoRico_Class28]
		[RicoRico_Class21]
		[RicoRico_Class24]
		[RicoRico_Class32]
		[RicoRico_Class29])
	(Plato_nombre "Pastel de chocolate")
	(Plato_orden 3)
	(Plato_precio 400)
	(Plato_tipos FAMILIAR)
	(Plato_tiposComida
		[RicoRico_Class41]
		[RicoRico_Class38]))

([RicoRico_Class58] of  Plato

	(Plato_complejidad 2)
	(Plato_epocas PRIMAVERA VERANO OTONO INVIERNO)
	(Plato_estiloComida [RicoRico_Class7])
	(Plato_ingredientes
		[RicoRico_Class34]
		[RicoRico_Class33])
	(Plato_nombre "Filete de ternera a la brasa")
	(Plato_orden 2)
	(Plato_precio 900)
	(Plato_propiedades [RicoRico_Class20077])
	(Plato_tipos FAMILIAR CONGRESO)
	(Plato_tiposComida [RicoRico_Class40]))

([RicoRico_Class7] of  EstiloComida

	(EstiloComida_nombre "Clasico"))

([RicoRico_Class8] of  EstiloComida

	(EstiloComida_nombre "Moderno"))

([RicoRico_Class9] of  EstiloComida

	(EstiloComida_nombre "Regional"))


)

(defglobal ?*tiempoInicial* = 0)

(defglobal ?*tiempoEjecucion* = 0)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;   Clases extras   ;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass Plato_recomendado
	(is-a USER)
	(role concrete)
    (slot plato
		(type INSTANCE)
		(create-accessor read-write))
    (slot afinidad
        (type INTEGER)
        (create-accessor read-write))
    (multislot satisfacciones
		(type STRING)
		(create-accessor read-write))
)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;   Modulos   ;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmodule MAIN "Modulo principal" 
    (export ?ALL)
)

(defmodule obtener_datos "Recopila los datos del evento"
	(import MAIN ?ALL)
	(export ?ALL)
)

(defmodule obtener_conocimiento "Obtiene la puntuacion de conocimiento"
	(import MAIN ?ALL)
	(export ?ALL)
)

(defmodule procesar_datos "Procesa los datos generando nuevas reglas"
	(import MAIN ?ALL)
	(import obtener_datos deftemplate ?ALL)
	(export ?ALL)
)

(defmodule genera_solucion "Genera una solucion con los datos generados"
	(import MAIN ?ALL)
    (import obtener_datos deftemplate ?ALL)
	(export ?ALL)
)

(defmodule muestra_solucion "Nos muestra la solucion que el sistema ha encontrado"
	(import MAIN ?ALL)
	(export ?ALL)
)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;   Deftemplates   ;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deftemplate MAIN::informacion_evento
	(slot numComensales (type INTEGER) (default -1)) ; NO restrictiva
    (slot precioMax (type INTEGER)(default -1)) ; restrictiva          
    (slot precioMin (type INTEGER)(default -1)) ; restrictiva
    (slot conocimiento (type INTEGER) (default 0))  ; NO restrictiva
    (slot bebidasAlcoholicas (type INTEGER) (default 1)) ; restrictiva bebidas
    (slot tipoEvento (type SYMBOL)) ; NO restrictiva
    (multislot tipoComida (type INSTANCE)) ; restrictiva
    (multislot estiloComidaPreferida (type INSTANCE))  ; NO restrictiva
    (multislot lugaresPreferidos (type INSTANCE))  ; NO restrictiva
    (slot epoca (type SYMBOL)) ; restrictiva
    (multislot propiedadesPreferidas (type INSTANCE)) ; NO restrictiva
    (multislot ingredientesProhibidos (type INSTANCE)) ; restrictiva
	(multislot platosPreferidos (type INSTANCE)) ; NO restrictiva
	
)

(deftemplate MAIN::listaBebidas
	(multislot bebidas (type INSTANCE))
)

(deftemplate MAIN::menus_propuestos
	(multislot menus_elegidos (type INSTANCE))
)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;   Funciones   ;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(deffunction MAIN::preguntar (?pregunta ?num) "Hace una pregunta y lee la respuesta (un unico valor)"
    (printout t crlf ?num "." ?pregunta crlf)
    (return (read))
)

(deffunction MAIN::preguntar_linea (?pregunta ?num) "Hace una pregunta y lee la respuesta (toda la linea)"
    (printout t crlf ?num "." ?pregunta crlf)
    (return (readline))
)

(deffunction MAIN::preguntar_lista (?pregunta ?lista ?num) "Hace una pregunta con una lista de opciones de las cuales podemos elegir varias"
    (while TRUE do 
        (printout t crlf ?num "." ?pregunta crlf)
        (printout t " (Puedes elegir mas de una opcion o no seleccionar ninguna)" crlf)
        (loop-for-count (?i 1 (length$ $?lista)) do
            (printout t "    " ?i "-" (nth$ ?i $?lista) crlf)
        )
        (bind ?linea (readline))
        (bind $?respuestas (explode$ ?linea))
        (bind ?numeros_escogidos (create$))
        (bind ?correct TRUE)
        (loop-for-count (?i 1 (length$ $?respuestas)) do
            (bind ?respuesta (nth$ ?i ?respuestas))
            (if (not (and(>= ?respuesta 1)(<= ?respuesta (length$ $?lista)))) then 
                (bind ?correct FALSE)
                (printout t "El valor " ?respuesta " no es valido" crlf)
            else
                (if (not (member ?respuesta $?numeros_escogidos)) then
                    (bind $?numeros_escogidos (insert$ $?numeros_escogidos (+ (length$ $?numeros_escogidos) 1) ?respuesta))
                )
            )
        )
		(if (eq ?correct TRUE) then (return $?numeros_escogidos))
    )
)

(deffunction MAIN::preguntar_lista_unica (?pregunta ?lista ?num) "Hace una pregunta con una lista de opciones de las cuales podemos elegir una"
    (while TRUE do 
        (printout t crlf ?num "." ?pregunta crlf)
    	(loop-for-count (?i 1 (length$ $?lista)) do
            (printout t "    " ?i ". " (nth$ ?i $?lista) crlf)
        )
        (bind ?respuesta (read))
		(if (not (and(>= ?respuesta 1)(<= ?respuesta (length$ $?lista)))) then 
                (printout t "El valor " ?respuesta " no es valido" crlf)
		else (return ?respuesta)
        )
	)

)

; Indices han de pertenecer a la lista pasada como parametro
(deffunction MAIN::get_sublista (?start ?final $?lista) "Devuelve la sublista de la lista que se pasa por paramatro entre los indices start y final"
    (bind $?sublista (create$))
    (loop-for-count (?i ?start ?final) do
        (bind ?elem (nth$ ?i $?lista))
        (bind $?sublista (insert$ $?sublista (+ (length$ $?sublista) 1) ?elem))
    )
    (return ?sublista)
)

(deffunction MAIN::comparisonPrice (?a ?b) "Compara el precio de dos menus. Devuelve cierto si el precio del primero es menor que el del segundo y falso sino"
    (< (send ?a get-Menu_precio) (send ?b get-Menu_precio))
)

(deffunction MAIN::comparisonIngredienteNombre (?a ?b) "Compara el nombre de dos ingredientes. Devuelve cierto si el nombre del primer ingrediente es mayor (lexicograficamente) que el del segundo y falso sino"
    (> (str-compare (send ?a get-Ingrediente_nombre) (send ?b get-Ingrediente_nombre)) 0)
)

(deffunction MAIN::comparisonPlatoNombre (?a ?b) "Compara el nombre de dos platos. Devuelve cierto si el nombre del primer plato es mayor (lexicograficamente) que el del segundo y falso sino"
    (> (str-compare (send ?a get-Plato_nombre) (send ?b get-Plato_nombre)) 0)
)

(deffunction MAIN::getMenuAfinidad (?menu) "Obtiene la afinidad de un menu pasado como parametro"
    (bind ?afinidad 
            (+ (send (send ?menu get-Menu_primero) get-afinidad)
            (+ (send (send ?menu get-Menu_segundo) get-afinidad)
               (send (send ?menu get-Menu_postre) get-afinidad))))
    (return ?afinidad)       
)

; Si hay más de un menu con maxima afinidad, para calidad 1 (caro) cogemos el más caro
; para calidad 3 (barato) el más barato y para calidad 2 (medio) el que tenga el precio medio
(deffunction MAIN::getMenuWithBestAfinity (?calidad $?menus) "Devuelve el menu con mas afinidad de una lista de entrada que contiene menus, la lista no es vacia"
    (bind ?bestMenu (nth$ 1 $?menus))
    (bind ?maxAfinidad (getMenuAfinidad ?bestMenu))
	(loop-for-count (?i 2 (length$ $?menus)) do
        (bind ?menu (nth$ ?i $?menus))
        (bind ?afinidadMenu (getMenuAfinidad ?menu))
        (if (eq ?calidad 3) then ; Si estamos escogiendo el barato, como mas barato mejor
            (if (>= ?afinidadMenu ?maxAfinidad) then
                (bind ?bestMenu ?menu)
                (bind ?maxAfinidad ?afinidadMenu)
            )
        else ; Si estamos escogiendo el caro, como mas caro mejor
            (if (> ?afinidadMenu ?maxAfinidad) then
                (bind ?bestMenu ?menu)
                (bind ?maxAfinidad ?afinidadMenu)
            )
        )
    )
    (if (eq ?calidad 2) then ; Si estamos escogiendo el medio, cogeremos el mas equilibrado posibles. El del medio
        (bind $?menusConMaximaAfinidad (create$))
        ; metemos en una lista los menus con la maxima afinidad obtenida
        (progn$ (?menu $?menus)
            (bind ?afinidadMenu (getMenuAfinidad ?menu))
            (if (eq ?afinidadMenu ?maxAfinidad) then
                (bind $?menusConMaximaAfinidad (insert$ $?menusConMaximaAfinidad (+ (length$ $?menusConMaximaAfinidad) 1) ?menu))
            )
        )
        ; y escogemos el del medio
        (if (eq (length$ ?menusConMaximaAfinidad) 1) then
            (bind ?bestMenu (nth$ 1 $?menusConMaximaAfinidad))
        else
            (bind ?bestMenu (nth$ (round (/ (length$ ?menusConMaximaAfinidad) 2)) $?menusConMaximaAfinidad))
        )
    )    
    (return ?bestMenu)
)

(deffunction MAIN::error (?errorText) "Saca un mensaje de error por pantalla en el formato adecuado"
    (printout t crlf "** ERRROR **" crlf)
    (printout t "< " ?errorText " >" crlf)
    (printout t "************" crlf crlf)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;   Message handlers   ;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Imprime una instancia de plato en un formato correcto
; Tambien imprime su precio y sus ingredientes
(defmessage-handler MAIN::Plato imprimirPlato ()
    (format t " * %s * %n" ?self:Plato_nombre)
    (format t "    Precio: %0.2f euros %n" (/ ?self:Plato_precio 100))
    (printout t "    Ingredientes:" crlf)
    (progn$ (?ingrediente ?self:Plato_ingredientes)
        (format t "      - %s %n" (send ?ingrediente get-Ingrediente_nombre))
    )
)

; Imprime una instancia de plato_recomendado en un formato correcto
; Tambien imprime su afinidad y los motivos de esta
(defmessage-handler MAIN::Plato_recomendado imprimirPlatoRecomendado ()
    (send ?self:plato imprimirPlato)
    (if (not (eq ?self:afinidad 0)) then
        (format t "    Con afinidad %d debido a:%n" ?self:afinidad)
        (progn$ (?motivo ?self:satisfacciones)
            (format t "      - %s %n" ?motivo)
        )
    )
)

; Imprime una instancia de bebida en un formato correcto
(defmessage-handler MAIN::Bebida imprimirBebida ()
    (format t " * %s *%n" ?self:Bebida_nombre)
    (if (eq ?self:Bebida_alcholica TRUE) then
        (printout t "    Bebida alcoholica: Si" crlf)
    else
        (printout t "    Bebida alcoholica: No" crlf)
    )
    (format t "    Precio: %0.2f euros%n%n" (/ ?self:Bebida_precio 100))
)

; Imprime una instancia de menu en un formato correcto
(defmessage-handler genera_solucion::Menu imprimirMenu ()
    (printout t crlf "Primer plato :" )
	(send ?self:Menu_primero imprimirPlatoRecomendado)
    (printout t crlf "Segundo plato :")  
	(send ?self:Menu_segundo imprimirPlatoRecomendado)
    (printout t crlf "Postre :")  
	(send ?self:Menu_postre imprimirPlatoRecomendado)
    (printout t crlf "Bebida :") 
    (send ?self:Menu_bebida imprimirBebida)
    (format t "Precio menu : %0.2f euros%n" (/ ?self:Menu_precio 100))
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;   MAIN   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defrule MAIN::inicio "Regla con la que inicia el programa"
    (declare (salience 100))
	=>
    (bind ?*tiempoInicial* (time))
	(printout t crlf "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" crlf)
  	(printout t      "%   Sistema de generacion de menus del restaurante RicoRico   %" crlf)
	(printout t      "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" crlf)
	(printout t crlf "Porfavor responde a las siguientes preguntas :D" crlf)
	(focus obtener_datos)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;  RECOGER DATOS   ;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; hechos iniciales que nos iran bien para ir resolviendo las preguntas
(deffacts obtener_datos::hechos_inicales
	(informacion_evento)
    (setTipoEvento)
    (setNumComensales)
    (setEpoca)
    (setMaxPrecio)
    (setMinPrecio)
    (setTipoComida)
    (setEstiloComida)
	(setLugares)
    (setPropiedadesPreferidas) 
    (setIngredientesProhibidos)
	(setbebidasAlcoholicas)
    (setPlatosPreferidos)
)

(defrule obtener_datos::pregunta1_set_tipo_evento "Establece el tipo de evento preguntandolo al usuario"
    (declare (salience 11))
    ?info <- (informacion_evento(tipoEvento ?tipoEvento))
    ?fact <- (setTipoEvento)
    =>
	(bind ?finish FALSE)
	(while (eq ?finish FALSE)
		(bind ?tipo (preguntar "Cual es el tipo del evento? Familiar[0]/Congreso[1]" 1))
		(switch ?tipo
			(case 0 then
				(modify ?info (tipoEvento FAMILIAR))
				(bind ?finish TRUE)
            )
			(case 1 then
				(modify ?info (tipoEvento CONGRESO))
				(bind ?finish TRUE)
            )
			(default
				(error (str-cat "El tipo de evento " ?tipo " no es valido"))
            )
        )
    )
    (retract ?fact)
)

(defrule obtener_datos::pregunta2_set_num_comensales "Establece el numero de comensales preguntandolo al usuario"
    (declare (salience 10))
    ?info <- (informacion_evento(numComensales ?comensales))
    ?fact <- (setNumComensales)
    =>
	(bind ?finish FALSE)
	(while (eq ?finish FALSE)
		(bind ?numComensales (preguntar "De cuantos comensales contara el evento?" 2))
		(if (> ?numComensales 0) then
			(modify ?info (numComensales ?numComensales))
			(bind ?finish TRUE)
		else
			(error (str-cat "El numero de comensales " ?numComensales " no puede ser menor que 1"))
        )
    )
    (retract ?fact)
)

(defrule obtener_datos::pregunta3_set_epoca "Establece la epoca del evento preguntandolo al usuario la fecha en que se realizara el evento"
    (declare (salience 9))
    ?info <- (informacion_evento)
    ?fact <- (setEpoca)
    =>
    (bind ?finish FALSE)
    (while (eq ?finish FALSE)
        (bind ?fecha (preguntar_linea "Indica la fecha en la que quieres realizar el evento. (ej. 24 5 2021)" 3))
        (bind $?dia_mes_ano (explode$ ?fecha))
        (if (not (eq (length $?dia_mes_ano) 3)) then 
            (error "Porfavor indica dia, mes y anyo")
            else
            (bind ?dia (nth$ 1 ?dia_mes_ano))
            (if (or (> ?dia 31) (< ?dia 1)) then
                (error (str-cat "El dia " ?dia " no es valido"))
             else
                (bind ?mes (nth$ 2 ?dia_mes_ano))
                (if (or (> ?mes 12) (< ?mes 1)) then
                    (error (str-cat "El mes " ?mes " no es valido"))
                 else
                    (bind ?ano (nth$ 3 ?dia_mes_ano))
                    (if (< ?ano 2021) then
                        (error (str-cat "El ano " ?ano " no es valido"))
                    else
                        ; dependiendo del mes ya sabemos si es una epoca u otra
                        ; si es un mes de cambio de estacion ponemos el bool a falso para que se compruebe el dia
                        (if (or (eq ?mes 1) (eq ?mes 2) ) then (assert (invierno TRUE ?dia)))
                        (if (or (eq ?mes 4) (eq ?mes 5) ) then (assert (primavera TRUE ?dia)))
                        (if (or (eq ?mes 7) (eq ?mes 8) ) then (assert (verano TRUE ?dia)))
                        (if (or (eq ?mes 10) (eq ?mes 11) ) then (assert (otono TRUE ?dia)))
                        (if (eq ?mes 3) then (assert (invierno FALSE ?dia)))
                        (if (eq ?mes 6) then (assert (primavera FALSE ?dia)))
                        (if (eq ?mes 9) then (assert (verano FALSE ?dia)))
                        (if (eq ?mes 12) then (assert (otono FALSE ?dia)))
                        (bind ?finish TRUE)
                    )
                )
            )
        )
    )
    (retract ?fact)
)

; Si el bool del hecho es false establece invierno o primavera dependiendo del dia del mes.
; Si el bool del hecho es cierto establece invierno
(defrule obtener_datos::invierno_primavera "Establece epoca invierno o primavera"
    (declare (salience 8))
    ?fact <- (invierno ?bool ?dia)
    ?info <- (informacion_evento)
    =>
    (if (eq ?bool TRUE) then
        (modify ?info(epoca INVIERNO))
        (assert (pregunta3_done))
    else (if (< ?dia 20) then (assert (invierno TRUE ?dia))
          else (assert (primavera TRUE ?dia))
         )
    )
    (retract ?fact)
)

; Si el bool del hecho es false establece primavera o verano dependiendo del dia del mes.
; Si el bool del hecho es cierto establece primavera
(defrule obtener_datos::primavera_verano "Establece epoca primavera o verano"
    (declare (salience 8))
    ?fact <- (primavera ?bool ?dia)
    ?info <- (informacion_evento)
    =>
    (if (eq ?bool TRUE) then
        (modify ?info(epoca PRIMAVERA))
        (assert (pregunta3_done))
    else (if (< ?dia 21) then (assert (primavera TRUE ?dia))
          else (assert (verano TRUE ?dia))
         )
    )
    (retract ?fact)
)

; Si el bool del hecho es false establece verano u otoño dependiendo del dia del mes.
; Si el bool del hecho es cierto establece verano
(defrule obtener_datos::verano_otono "Establece epoca verano u otono"
    (declare (salience 8))
    ?fact <- (verano ?bool ?dia)
    ?info <- (informacion_evento)
    =>
    (if (eq ?bool TRUE) then
        (modify ?info(epoca VERANO))
        (assert (pregunta3_done))
    else (if (< ?dia 22) then (assert (verano TRUE ?dia))
          else (assert (otono TRUE ?dia))
         )
    )
    (retract ?fact)
)

; Si el bool del hecho es false establece otoño o invierno dependiendo del dia del mes.
; Si el bool del hecho es cierto establece otoño
(defrule obtener_datos::otono_invierno "Establece epoca otono o invierno"
    (declare (salience 8))
    ?fact <- (otono ?bool ?dia)
    ?info <- (informacion_evento)
    =>
    (if (eq ?bool TRUE) then
        (modify ?info(epoca OTONO))
        (assert (pregunta3_done))
    else (if (< ?dia 21) then (assert (otono TRUE ?dia))
          else (assert (invierno TRUE ?dia))
         )
    )
    (retract ?fact)
)

(defrule obtener_datos::pregunta4_set_max_precio "Establece el maximo precio por menu del evento preguntandolo al usuario"
    (declare (salience 7))
    ?info <- (informacion_evento)
    ?fact <- (setMaxPrecio)
    =>
	(bind ?finish FALSE)
	(while (eq ?finish FALSE)
		(bind ?max (preguntar "Indica el maximo precio (euros) que estas dispuesto a pagar por menu" 4))
		(if (>= ?max 1) then
			(modify ?info (precioMax (* ?max 100)))
			(bind ?finish TRUE)
		else
			(error (str-cat "El precio maximo " ?max " no puede ser menor que 1"))
        )
    )
    (retract ?fact)
)

(defrule obtener_datos::pregunta5_set_min_precio "Establece el minimo precio por menu del evento preguntandolo al usuario"
    (declare (salience 6))
    ?info <- (informacion_evento(precioMax ?precioMax))
    ?fact <- (setMinPrecio)
    =>
	(bind ?finish FALSE)
	(while (eq ?finish FALSE)
		(bind ?min (preguntar "Indica el minimo precio (euros) que estas dispuesto a pagar por menu" 5))
		(if (>= ?min 1) then
			(bind ?min (* ?min 100))
			(if (> ?min ?precioMax) then
				(error (str-cat "El precio minimo ha de ser menor o igual al precio maximo escogido"))
			else 
				(modify ?info (precioMin ?min))
				(bind ?finish TRUE)
            )
		else
			(error "El precio minimo no puede ser menor que 1")
        )
    )
    (retract ?fact)
)

(defrule obtener_datos::pregunta6_set_tipo_comida "Establece el tipo de la comida del evento preguntandolo al usuario"
	(declare (salience 5))
    ?info <- (informacion_evento)
    ?fact <- (setTipoComida)
	=>
	(bind $?nombre_todos_tipos (create$))
    ; cogemos todas las instancias de tipoComida
	(bind $?lista_todos_tipos (find-all-instances ((?tipo TipoComida)) TRUE))
	
    ; metemos los nombres de estos tipos en una lista
	(loop-for-count (?i 1 (length$ $?lista_todos_tipos)) do
		(bind ?j (nth$ ?i ?lista_todos_tipos))
		(bind ?nombre_tipo (send ?j get-TipoComida_nombre))
		(bind $?nombre_todos_tipos (insert$ $?nombre_todos_tipos (+ (length$ $?nombre_todos_tipos) 1) ?nombre_tipo))
	)
    ; preguntamos al usuario los que prefiere
	(bind $?numero_tipos_escogidos (preguntar_lista "Escoge los tipos de comida que prefieras (son restrictivos)" $?nombre_todos_tipos 6))
	
	(bind $?tipos_escogidos (create$))
    ; añadimos a una lista vacía los tipos de comida que el usuario ha escogido gracias al identificador
	(loop-for-count (?i 1 (length$ ?numero_tipos_escogidos)) do
	 	(bind ?j (nth$ ?i ?numero_tipos_escogidos))
	 	(bind ?tipo_escogido (nth$ ?j ?lista_todos_tipos))
	 	(bind $?tipos_escogidos (insert$ $?tipos_escogidos (+ (length$ $?tipos_escogidos) 1) ?tipo_escogido))
	)
	(modify ?info (tipoComida $?tipos_escogidos))
    (retract ?fact)
)

; ver regla pregunta6_set_tipo_comida para más comentarios
(defrule obtener_datos::pregunta7_set_estilo_comida "Establece el estilo de la comida del evento preguntandolo al usuario"
	(declare (salience 4))
    ?info <- (informacion_evento (numComensales ?numComensales))
    ?fact <- (setEstiloComida)
	=>
	(bind $?nombre_todos_estilos (create$))
	(bind $?lista_todos_estilos (find-all-instances ((?estilo EstiloComida)) TRUE))
    (if (>= ?numComensales 50) then
        (bind $?lista_todos_estilos (find-all-instances ((?estilo EstiloComida)) (not (eq "Sibarita" ?estilo:EstiloComida_nombre))))
    )
	
	(loop-for-count (?i 1 (length$ $?lista_todos_estilos)) do
		(bind ?j (nth$ ?i ?lista_todos_estilos))
		(bind ?nombre_estilo (send ?j get-EstiloComida_nombre))
		(bind $?nombre_todos_estilos (insert$ $?nombre_todos_estilos (+ (length$ $?nombre_todos_estilos) 1) ?nombre_estilo))
	)
	(bind $?numero_estilos_escogidos (preguntar_lista "Escoge los estilos de comida que prefieras" $?nombre_todos_estilos 7))
	
	(bind $?estilos_escogidos (create$))
	(loop-for-count (?i 1 (length$ ?numero_estilos_escogidos)) do
	 	(bind ?j (nth$ ?i ?numero_estilos_escogidos))
	 	(bind ?estilo_escogido (nth$ ?j ?lista_todos_estilos))
	 	(bind $?estilos_escogidos (insert$ $?estilos_escogidos (+ (length$ $?estilos_escogidos) 1) ?estilo_escogido))
        ; asertamos esRegional para hacer o no la pregunta 7.1
		(if (eq "Regional" (nth$ ?j ?nombre_todos_estilos)) then
			(assert (esRegional))
        )
	)
	(modify ?info (estiloComidaPreferida $?estilos_escogidos))
    (retract ?fact)
)

; ver regla pregunta6_set_tipo_comida para más comentarios
(defrule obtener_datos::pregunta7_1_set_lugares "Establece los lugares de preferencia para los platos regionales"
	(declare (salience 4))
    ?info <- (informacion_evento)
    ?fact <- (setLugares)
    (esRegional)
	=>
	(bind $?nombre_todos_lugares (create$))
	(bind $?lista_todos_lugares (find-all-instances ((?lugar Lugar)) TRUE))
	
	(loop-for-count (?i 1 (length$ $?lista_todos_lugares)) do
		(bind ?j (nth$ ?i ?lista_todos_lugares))
		(bind ?nombre_lugar (send ?j get-Lugar_nombre))
		(bind $?nombre_todos_lugares (insert$ $?nombre_todos_lugares (+ (length$ $?nombre_todos_lugares) 1) ?nombre_lugar))
	)
	(bind $?numero_lugares_escogidos (preguntar_lista "Escoge los lugares de comida regional que prefieras" $?nombre_todos_lugares 7.1))
	
	(bind $?lugares_escogidos (create$))
	(loop-for-count (?i 1 (length$ ?numero_lugares_escogidos)) do
	 	(bind ?j (nth$ ?i ?numero_lugares_escogidos))
	 	(bind ?lugar_escogido (nth$ ?j ?lista_todos_lugares))
	 	(bind $?lugares_escogidos (insert$ $?lugares_escogidos (+ (length$ $?lugares_escogidos) 1) ?lugar_escogido))
	)
	(modify ?info (lugaresPreferidos $?lugares_escogidos))
    (retract ?fact)
)

; ver regla pregunta6_set_tipo_comida para más comentarios
(defrule obtener_datos::pregunta8_set_propiedades_comida "Establece las propiedades extras que prefiera de la comida del evento"
	(declare (salience 3))
    ?info <- (informacion_evento)
    ?fact <- (setPropiedadesPreferidas)
	=>
	(bind $?nombre_todas_props (create$))
	(bind $?lista_todas_props (find-all-instances ((?prop Propiedad)) TRUE))
	
	(loop-for-count (?i 1 (length$ $?lista_todas_props)) do
		(bind ?j (nth$ ?i ?lista_todas_props))
		(bind ?nombre_prop (send ?j get-Propiedad_nombre))
		(bind $?nombre_todas_props (insert$ $?nombre_todas_props (+ (length$ $?nombre_todas_props) 1) ?nombre_prop))
	)
	(bind $?numero_props_escogidas (preguntar_lista "Escoge las propiedades extras que prefieras" $?nombre_todas_props 8))
	
	(bind $?props_escogidas (create$))
	(loop-for-count (?i 1 (length$ ?numero_props_escogidas)) do
	 	(bind ?j (nth$ ?i ?numero_props_escogidas))
	 	(bind ?prop_escogida (nth$ ?j ?lista_todas_props))
	 	(bind $?props_escogidas (insert$ $?props_escogidas (+ (length$ $?props_escogidas) 1) ?prop_escogida))
	)
	(modify ?info (propiedadesPreferidas $?props_escogidas))
    (retract ?fact)
)

; ver regla pregunta6_set_tipo_comida para más comentarios
; aunque en esta regla primero ordenamos los ingredientes por nombre para poder visualizarlos por orden alfabetico
(defrule obtener_datos::pregunta9_set_ingredientes_prohibidos "Establece los ingredientes prohibidos del evento"
    (declare (salience 2))
    ?info <- (informacion_evento)
    ?fact <- (setIngredientesProhibidos)
	=>
	(bind $?nombre_todos_ingredientes (create$))
	(bind $?lista_todos_ingredientes (find-all-instances ((?ingrediente Ingrediente)) TRUE))
    
	(bind $?lista_todos_ingredientes (sort comparisonIngredienteNombre $?lista_todos_ingredientes))
    
	(loop-for-count (?i 1 (length$ $?lista_todos_ingredientes)) do
		(bind ?j (nth$ ?i ?lista_todos_ingredientes))
		(bind ?nombre_ingrediente (send ?j get-Ingrediente_nombre))
		(bind $?nombre_todos_ingredientes (insert$ $?nombre_todos_ingredientes (+ (length$ $?nombre_todos_ingredientes) 1) ?nombre_ingrediente))
	)
	(bind $?numero_ingredientes_escogidos (preguntar_lista "Escoge los ingredientes que quieres prohibir en el evento" $?nombre_todos_ingredientes 9))
	
	(bind $?ingredientes_escogidos (create$))
	(loop-for-count (?i 1 (length$ ?numero_ingredientes_escogidos)) do
	 	(bind ?j (nth$ ?i ?numero_ingredientes_escogidos))
	 	(bind ?ingrediente_escogido (nth$ ?j ?lista_todos_ingredientes))
	 	(bind $?ingredientes_escogidos (insert$ $?ingredientes_escogidos (+ (length$ $?ingredientes_escogidos) 1) ?ingrediente_escogido))
	)
	(modify ?info (ingredientesProhibidos $?ingredientes_escogidos))
    (retract ?fact)
)

(defrule obtener_datos::pregunta10_set_permiso_alcohol "Establece si estan permitidas (o no) las bebidas alcoholicas en el evento"
    (declare (salience 1))
    ?info <- (informacion_evento(bebidasAlcoholicas ?bebidasAlcoholicas))
    ?fact <- (setbebidasAlcoholicas)
    =>
	(bind ?finish FALSE)
	(while (eq ?finish FALSE)
		(bind ?permiso (preguntar "Quieres permitir bebidas alcoholicas en el evento? No[0]/Si[1]" 10))
		(switch ?permiso
			(case 0 then
				(modify ?info (bebidasAlcoholicas 0))
				(bind ?finish TRUE)
            )
			(case 1 then
				(modify ?info (bebidasAlcoholicas 1))
				(bind ?finish TRUE)
            )
			(default
				(error (str-cat "La opcion " ?permiso " no es valida"))
            )
        )
    )
    (retract ?fact)
)

; ver regla pregunta6_set_tipo_comida para más comentarios
; aunque en esta regla primero ordenamos los platos por nombre para poder visualizarlos por orden alfabetico
(defrule obtener_datos::pregunta11_set_platos_preferidos "Establece los platos preferidos del evento"
    ?info <- (informacion_evento)
    ?fact <- (setPlatosPreferidos)
	=>
	(bind $?nombre_todos_platos (create$))
	(bind $?lista_todos_platos (find-all-instances ((?plato Plato)) TRUE))
	
    (bind $?lista_todos_platos (sort comparisonPlatoNombre $?lista_todos_platos))
	
    (loop-for-count (?i 1 (length$ $?lista_todos_platos)) do
		(bind ?j (nth$ ?i ?lista_todos_platos))
		(bind ?nombre_plato (send ?j get-Plato_nombre))
		(bind $?nombre_todos_platos (insert$ $?nombre_todos_platos (+ (length$ $?nombre_todos_platos) 1) ?nombre_plato))
	)
	(bind $?numero_platos_escogidos (preguntar_lista "Escoge los platos que prefieras que aparezcan" $?nombre_todos_platos 11))
	
	(bind $?platos_escogidos (create$))
	(loop-for-count (?i 1 (length$ ?numero_platos_escogidos)) do
	 	(bind ?j (nth$ ?i ?numero_platos_escogidos))
	 	(bind ?plato_escogido (nth$ ?j ?lista_todos_platos))
	 	(bind $?platos_escogidos (insert$ $?platos_escogidos (+ (length$ $?platos_escogidos) 1) ?plato_escogido))
	)
	(modify ?info (platosPreferidos $?platos_escogidos))
    (retract ?fact)
)

(defrule obtener_datos::todas_preguntas_done "Cambiamos de modulo al acabar todas las preguntas"
    (declare (salience -1))
    =>
    (focus obtener_conocimiento)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;  OBTENER CONOCIMIENTO   ;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defrule obtener_conocimiento::preguntas_conocimiento "Hacemos las preguntas de conocimiento"
    (declare (salience 10))
	?con <- (informacion_evento(conocimiento ?conocimiento))
    (not (setConocimiento))
	=>
	(printout t crlf "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" crlf)
    (printout t      "%           Ahora evaluaremos su conocimiento culinario           %" crlf)
    (printout t      "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" crlf)
    (printout t      "%             Le haremos una serie de preguntas, a las            %" crlf)
    (printout t      "%               cuales debera responder con 1,2,3,4               %" crlf)
    (printout t      "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" crlf)
	
	(bind ?opciones (create$ "El origen de los huevos" "Son iguales" "El acompanamiento" "Las patatas como ingrediente"))
	(bind ?respuesta (preguntar_lista_unica "En que se diferencia la tortilla espanola y francesa?" ?opciones 1))
	(if (eq ?respuesta 4) then (bind ?conocimiento (+ 1 ?conocimiento)))
	
	(bind ?opciones (create$ "El pan de Viena" "El chocolate" "El roast-beef" "Las tempuras"))
	(bind ?respuesta (preguntar_lista_unica "Cual de los siguientes alimentos es tipico de Suiza" ?opciones 2))
	(if (eq ?respuesta 2) then (bind ?conocimiento (+ 1 ?conocimiento)))

	(bind ?opciones (create$ "Un tipo de foie de pato" "Un punto de coccion de la carne" "Un tipo de salsa" "Una forma minimalista de presentar los platos"))
	(bind ?respuesta (preguntar_lista_unica "La reduccion de Pedro Ximenez es..." ?opciones 3))
	(if (eq ?respuesta 3) then (bind ?conocimiento (+ 1 ?conocimiento)))

	(bind ?opciones (create$ "Suizo" "Polaco" "Aleman" "Neozelandes"))
	(bind ?respuesta (preguntar_lista_unica "El Bratwurst hace referencia a una variedad de salchichas de origen..." ?opciones 4))
	(if (eq ?respuesta 3) then (bind ?conocimiento (+ 1 ?conocimiento)))

	(bind ?opciones (create$ "Pan blanco" "Pan acimo" "Baguette" "Pan germinado"))
	(bind ?respuesta (preguntar_lista_unica "Como se llama el pan sin levadura?" ?opciones 5))
	(if (eq ?respuesta 2) then (bind ?conocimiento (+ 1 ?conocimiento)))

	(bind ?opciones (create$ "Escoces" "Irlandes" "Australiano" "Frances"))
	(bind ?respuesta (preguntar_lista_unica "El champ es un pure de patatas de origen..." ?opciones 6))
	(if (eq ?respuesta 2) then (bind ?conocimiento (+ 1 ?conocimiento)))

	(bind ?opciones (create$ "India" "Turquia" "Pakistan" "Irlanda"))
	(bind ?respuesta (preguntar_lista_unica "El pollo tikka es una especialidad de..." ?opciones 7))
	(if (eq ?respuesta 1) then (bind ?conocimiento (+ 1 ?conocimiento)))

	(bind ?opciones (create$ "Inglesa" "Francesa" "Canadiense" "Suiza"))
	(bind ?respuesta (preguntar_lista_unica "La poutine es un plato de la gastronomia..." ?opciones 8))
	(if (eq ?respuesta 3) then (bind ?conocimiento (+ 1 ?conocimiento)))

	(bind ?opciones (create$ "Brasileno" "Mexicano" "Argentino" "Dominicano"))
	(bind ?respuesta (preguntar_lista_unica "El mole hace referencia a una variedad de salsas de origen..." ?opciones 9))
	(if (eq ?respuesta 2) then (bind ?conocimiento (+ 1 ?conocimiento)))

	(bind ?opciones (create$ "Polaca" "Alemana" "Austriaca" "Finlandesa"))
	(bind ?respuesta (preguntar_lista_unica "El Wiener Schnitzel es uno de los platos mas conocidos de la cocina..." ?opciones 10))
	(if (eq ?respuesta 3) then (bind ?conocimiento (+ 1 ?conocimiento)))
    
	(modify ?con (conocimiento ?conocimiento))
	(assert (setConocimiento))
    
    (printout t crlf "Todos los datos del usuario obtenidos con exito" crlf)
    (printout t "Procedemos a procesar los datos" crlf)
    (printout t "Conocimiento del usuario: " ?conocimiento crlf)
    (bind ?*tiempoEjecucion* (time))
	(focus procesar_datos)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;  PROCESAR DATOS   ;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defrule procesar_datos::crea_instancias_platos "Crea instancias de todos los platos que cumplen las restricciones"
	(informacion_evento (tipoEvento ?tipoEvento) (estiloComidaPreferida $?estiloComida) (tipoComida $?tipoComida) (epoca ?epoca) (ingredientesProhibidos $?ingredientesProhibidos) )
	?plato <-(object (is-a Plato))
    (not (instancias_plato_done ?plato)) ;para que no vuelva a entrar
	=>
    (bind ?satisface TRUE)
    
    ;control tipo de evento
    ;si alguno de los eventos del plato es igual el evento escogido entonces pasa este filtro
    (bind ?aux FALSE)
    (bind $?tiposEventos_plato (send ?plato get-Plato_tipos))
    (progn$ (?tipoEvento_plato $?tiposEventos_plato)
        (if (eq ?tipoEvento_plato ?tipoEvento) then (bind ?aux TRUE))
    )
    (if (eq ?aux FALSE) then (bind ?satisface FALSE))
    
    ;control epoca evento
    ;si alguna de las epocas del plato es igual a la epoca escogida entonces pasa este filtro 
    (bind ?aux FALSE)
    (bind $?epocas_plato (send ?plato get-Plato_epocas))
    (progn$ (?epoca_plato $?epocas_plato)
        (if (eq ?epoca_plato ?epoca) then (bind ?aux TRUE))
    )
    (if (eq ?aux FALSE) then (bind ?satisface FALSE))
    
    ;control tipo comida
    ;si alguno de los tipos de comida escogidos no pertenece a los tipos de comida del plato entonces no pasa el filtro
    (bind ?aux TRUE)
    (bind $?tiposComida_plato (send ?plato get-Plato_tiposComida))
    (progn$ (?tipoComida_escogido $?tipoComida)
        (if (not (member ?tipoComida_escogido $?tiposComida_plato)) then
            (bind ?aux FALSE)
        )
    )
    (if (eq ?aux FALSE) then (bind ?satisface FALSE))
    
    ;control ingredientes prohibidos
    ;si alguna de los ingredientes prohibidos pertenece a los ingredientes del plato entonces no pasa este filtro 
    (bind ?aux TRUE)
    (bind $?ingredientes_plato (send ?plato get-Plato_ingredientes))
    (progn$ (?ingrediente_plato $?ingredientes_plato)
        (if (member ?ingrediente_plato $?ingredientesProhibidos) then (bind ?aux FALSE))
    )
    (if (eq ?aux FALSE) then (bind ?satisface FALSE))
    
    ;si hemos pasado todos los filtros creamos una instancia de un plato recomendado con este plato y afinidad 0
    (if (eq ?satisface TRUE) then (make-instance (gensym) of Plato_recomendado (plato ?plato) (afinidad 0)))
)

(defrule procesar_datos::hechos_platos "Creamos hechos con los platos que han sido escogidos como preferidos"
    (informacion_evento (platosPreferidos $?platos))
    (not (hechos_platos_done))
    =>
    (progn$ (?plato $?platos)
        (assert (plato_escogido ?plato))
    )
    (assert (hechos_platos_done))
)

(defrule procesar_datos::hechos_lugares "Creamos hechos con los lugares que han sido escogidos como preferidos"
    (informacion_evento (lugaresPreferidos $?lugares))
    (not (hechos_lugares_done))
    =>
    (progn$ (?lugar $?lugares)
        (assert (lugar_escogido ?lugar))
    )
    (assert (hechos_lugares_done))
)

(defrule procesar_datos::hechos_estilos "Creamos hechos con los estilos que han sido escogidos como preferidos"
    (informacion_evento (estiloComidaPreferida $?estilos))
    (not (hechos_estilos_done))
    =>
    (progn$ (?estilo $?estilos) 
        (assert (estilo_escogido ?estilo))
    )
    (assert (hechos_estilos_done))
)

(defrule procesar_datos::hechos_propiedades "Creamos hechos con las propiedades que han sido escogidas como preferidas"
    (informacion_evento (propiedadesPreferidas $?propiedades))
    (not (hechos_propiedas_done))
    =>
    (progn$ (?propiedad $?propiedades) 
        (assert (propiedad_escogida ?propiedad))
    )
    (assert (hechos_propiedas_done))
)

; para cada plato recomendado aumentamos su afinidad si tiene la propiedad escogida como preferida
(defrule procesar_datos::aumentar_afinidad_propiedades "Aumentamos la afinidad de los platos segun las propiedades preferidas"
    (propiedad_escogida ?prop)
	?plato_recomendado <-(object (is-a Plato_recomendado) (plato ?plato) (afinidad ?afinidad) (satisfacciones $?satisfacciones))
    (not (afinidad_plato_propiedad_done ?plato ?prop))
    (test (member ?prop (send ?plato get-Plato_propiedades)))
	=>
	(bind ?afinidad (+ ?afinidad 1 ))
    (send ?plato_recomendado put-afinidad ?afinidad)   
    
	(bind ?motivo (str-cat "Aumentamos 1 punto de afinidad por tener la propiedad escogida: " (send ?prop get-Propiedad_nombre)))
    (bind $?satisfacciones (insert$ $?satisfacciones (+ (length$ $?satisfacciones) 1) ?motivo))
    (send ?plato_recomendado put-satisfacciones $?satisfacciones)

	(assert (afinidad_plato_propiedad_done ?plato ?prop))
)

; para cada plato recomendado aumentamos su afinidad si tiene el estilo escogido como preferido
(defrule procesar_datos::aumentar_afinidad_estilos "Aumentamos la afinidad de los platos segun los estilos preferidos"
    (estilo_escogido ?estilo)
	?plato_recomendado <-(object (is-a Plato_recomendado) (plato ?plato) (afinidad ?afinidad) (satisfacciones $?satisfacciones))
    (not (afinidad_plato_estilo_done ?plato ?estilo))
    (test (eq ?estilo (send ?plato get-Plato_estiloComida)))
	=>
	(bind ?afinidad (+ ?afinidad 1 ))
    (send ?plato_recomendado put-afinidad ?afinidad)   
    
	(bind ?motivo (str-cat "Aumentamos 1 punto de afinidad por tener el estilo escogido: " (send ?estilo get-EstiloComida_nombre)))
    (bind $?satisfacciones (insert$ $?satisfacciones (+ (length$ $?satisfacciones) 1) ?motivo))
    (send ?plato_recomendado put-satisfacciones $?satisfacciones)

	(assert (afinidad_plato_estilo_done ?plato ?estilo))
)

; para cada plato recomendado aumentamos su afinidad si tiene el lugar de origen escogido como preferido
(defrule procesar_datos::aumentar_afinidad_lugares "Aumentamos la afinidad de los platos segun los lugares preferidos"
    (lugar_escogido ?lugar)
	?plato_recomendado <-(object (is-a Plato_recomendado) (plato ?plato) (afinidad ?afinidad) (satisfacciones $?satisfacciones))
    (not (afinidad_plato_lugar_done ?plato ?lugar))
    (test (eq ?lugar (send ?plato get-Plato_origen)))
	=>
	(bind ?afinidad (+ ?afinidad 1 ))
    (send ?plato_recomendado put-afinidad ?afinidad)   
    
	(bind ?motivo (str-cat "Aumentamos 1 punto de afinidad por ser del lugar escogido: " (send ?lugar get-Lugar_nombre)))
    (bind $?satisfacciones (insert$ $?satisfacciones (+ (length$ $?satisfacciones) 1) ?motivo))
    (send ?plato_recomendado put-satisfacciones $?satisfacciones)
    
	(assert (afinidad_plato_lugar_done ?plato ?lugar))
)

; para cada plato recomendado aumentamos su afinidad si es un plato escogido como preferido
(defrule procesar_datos::aumentar_afinidad_platos "Aumentamos la afinidad de los platos segun los platos preferidos escogidos"
    (plato_escogido ?platoEscogido)
	?plato_recomendado <-(object (is-a Plato_recomendado) (plato ?plato) (afinidad ?afinidad) (satisfacciones $?satisfacciones))
    (not (afinidad_plato_preferido_done ?plato ?platoEscogido))
    (test (eq ?platoEscogido ?plato))
	=>
	(bind ?afinidad (+ ?afinidad 1 ))
    (send ?plato_recomendado put-afinidad ?afinidad)   
    
	(bind ?motivo (str-cat "Aumentamos 1 punto de afinidad por ser un plato preferido: " (send ?platoEscogido get-Plato_nombre)))
    (bind $?satisfacciones (insert$ $?satisfacciones (+ (length$ $?satisfacciones) 1) ?motivo))
    (send ?plato_recomendado put-satisfacciones $?satisfacciones)

	(assert (afinidad_plato_preferido_done ?plato ?platoEscogido))
)

; para cada plato recomendado aumentamos su afinidad si:
;   su conocimiento es bajo y la complejidad del plato tambien
;   su conocimiento es medio y la complejidad del plato tambien
;   su conocimiento es alto y la complejidad del plato tambien
(defrule procesar_datos::aumentar_afinidad_conocimiento "Aumentamos la afinidad de los platos segun su complejidad y conocimiento del usuario"
    (informacion_evento (conocimiento ?conocimiento_evento))
	?plato_recomendado <-(object (is-a Plato_recomendado) (plato ?plato) (afinidad ?afinidad) (satisfacciones $?satisfacciones))
    (not (afinidad_plato_conocimiento_done ?plato))
	=>
    (bind ?value 0)
	(if (and (>= ?conocimiento_evento 0)(<= ?conocimiento_evento 3)) then 
		(if (eq (send ?plato get-Plato_complejidad) 1) then (bind ?value 1))
	)
	(if (and (> ?conocimiento_evento 3)(<= ?conocimiento_evento 7)) then 
		(if (eq (send ?plato get-Plato_complejidad) 2) then (bind ?value 2))		
	)
	(if (and (> ?conocimiento_evento 7)(<= ?conocimiento_evento 10)) then 
		(if (eq (send ?plato get-Plato_complejidad) 3) then (bind ?value 3))
	)
    (if (not (eq ?value 0)) then
        (bind ?afinidad (+ ?afinidad 1 ))
		(send ?plato_recomendado put-afinidad ?afinidad)
        (switch ?value
			(case 1 then (bind ?motivo (str-cat "Aumentamos 1 punto de afinidad porque tanto complejidad del plato como el conocimiento del usuario son bajos" )))
			(case 2 then (bind ?motivo (str-cat "Aumentamos 1 punto de afinidad porque tanto complejidad del plato como el conocimiento del usuario son medios" )))
            (case 3 then (bind ?motivo (str-cat "Aumentamos 1 punto de afinidad porque tanto complejidad del plato como el conocimiento del usuario son elevados" )))
        )
    	(bind $?satisfacciones (insert$ $?satisfacciones (+ (length$ $?satisfacciones) 1) ?motivo))
    	(send ?plato_recomendado put-satisfacciones $?satisfacciones)
    )
	(assert (afinidad_plato_conocimiento_done ?plato))
)

(defrule procesar_datos::informacion_procesada "Ya hemos procesado la informacion. Cambiamos de modulo"
    (declare (salience -1))
    =>
    (printout t "Modulo de procesamiento de datos finalizado" crlf)
    (focus genera_solucion)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;  GENERAR SOLUCION   ;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defrule genera_solucion::sin_solucion "Indica que no hemos podido generar 3 menus"
    (declare (salience 50))
    ?fact <- (no3Menus)
    (not (halted))
	=>
    (printout t "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" crlf)
    (printout t "% No hemos podido generar 3 menus con las restricciones indicadas %" crlf)
    (printout t "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" crlf)
    (assert (halted))
)

(defrule genera_solucion::primera_regla_genera_solucion "Primera regla que inicializa un vector de platos"
    (declare (salience 10))
    =>
    (printout t "Procedemos a generar una solucion" crlf)
    (bind ?bebidas (create$))
    (assert (listaBebidas (bebidas ?bebidas)))
)

; creamos lista con las bebidas (no añadimos las que sea alcoholicas si el usuario no quería este tipo de bebidas)
(defrule genera_solucion::crear_lista_con_bebidas_recomendadas "Creamos lista con todas las bebidas recomendadas"
    (declare (salience 9))
    (informacion_evento (bebidasAlcoholicas ?alchol))
    ?bebida_recomendada <-(object (is-a Bebida) (Bebida_alcholica ?esAlcholica))
    ?lista_todas_bebidas_recomendadas <- (listaBebidas (bebidas $?bebidas))
    (not (bebida_anadida ?bebida_recomendada))
    ; si se permite el alcohol añadimos
    ; o
    ; si no es alcoholica añadimos
    (test (or (eq ?alchol 1) (eq ?esAlcholica FALSE)))
    =>
    (bind $?bebidas (insert$ $?bebidas (+ (length$ $?bebidas) 1) ?bebida_recomendada))
    (modify ?lista_todas_bebidas_recomendadas (bebidas $?bebidas))
    (assert (bebida_anadida ?bebida_recomendada))
)

(defrule genera_solucion::crea_trios "Creamos todos los trios de platos posibles teniendo en cuenta su orden dentro del menu"
    (declare (salience 8))
    ?lista_todas_bebidas_recomendadas <- (listaBebidas (bebidas $?bebidas))
    =>
    (bind $?platos (find-all-instances ((?inst Plato_recomendado)) TRUE))
    (printout t "Hemos encontrado " (length$ $?platos) " platos adecuados" crlf)
    (printout t "Hemos encontrado " (length$ $?bebidas) " bebidas adecuadas" crlf)
    (progn$ (?plato1 ?platos)
        (if (eq 1 (send (send ?plato1 get-plato) get-Plato_orden)) then
            (progn$ (?plato2 ?platos)
                (if (eq 2 (send (send ?plato2 get-plato) get-Plato_orden)) then
                    (progn$ (?plato3 ?platos)
                        (if (eq 3 (send (send ?plato3 get-plato) get-Plato_orden)) then
                            (assert (posibleMenu ?plato1 ?plato2 ?plato3))
                        )
                    )
                )
            )
        )
    )
)


(defrule genera_solucion::checkComptibility "Comprueba las compatibilidades de los platos"
    (declare (salience 7))
    ?fact <- (posibleMenu ?platoRec1 ?platoRec2 ?platoRec3)
    =>
    (bind ?plato1Nombre (send (send ?platoRec1 get-plato) get-Plato_nombre))
    (bind ?plato2Nombre (send (bind ?plato2 (send ?platoRec2 get-plato)) get-Plato_nombre))
    (bind ?plato3 (send ?platoRec3 get-plato))
    
    (bind ?noCompPlato2 (create$))
    (bind $?lista (send ?plato2 get-Plato_noCompatibilidad))
    (progn$ (?elem ?lista)
        (bind $?noCompPlato2 (insert$ $?noCompPlato2 (+ (length$ $?noCompPlato2) 1) (send ?elem get-Plato_nombre)))
    )
    
    (bind ?noCompPlato3 (create$))
    (bind $?lista (send ?plato3 get-Plato_noCompatibilidad))
    (progn$ (?elem ?lista)
        (bind $?noCompPlato3 (insert$ $?noCompPlato3 (+ (length$ $?noCompPlato3) 1) (send ?elem get-Plato_nombre)))
    )
    
    ; tuvimos que crear las listas de nombres nosotros porque tuvimos problemas con las instancias y el member en listas de instancias (o nombres de instancias)
    (if (and (not (member ?plato1Nombre ?noCompPlato2)) 
        (and (not (member ?plato1Nombre ?noCompPlato3))
             (not (member ?plato2Nombre ?noCompPlato3)))) then
        (assert (posibleMenuCompatible ?platoRec1 ?platoRec2 ?platoRec3))
    )
    (retract ?fact)
)


(defrule genera_solucion::checkCompatibilityBebida "Para cada bebida i posible menu añadimos la bebida a esos menus comprobando que esta es compatible"
    (declare (salience 6))
    ?lista_todas_bebidas_recomendadas <- (listaBebidas (bebidas $?bebidas))
    ?fact <- (posibleMenuCompatible ?platoRec1 ?platoRec2 ?platoRec3)
    =>
    (progn$ (?bebida ?bebidas)
        (bind ?noCompBebida (create$))
        (bind $?lista (send ?bebida get-Bebida_noCompatibilidad))
        (progn$ (?elem ?lista)
            (bind $?noCompBebida (insert$ $?noCompBebida (+ (length$ $?noCompBebida) 1) (send ?elem get-Plato_nombre)))
        )
        (bind ?plato1Nombre (send (send ?platoRec1 get-plato) get-Plato_nombre))
        (bind ?plato2Nombre (send (send ?platoRec2 get-plato) get-Plato_nombre))
        (bind ?plato3Nombre (send (send ?platoRec3 get-plato) get-Plato_nombre))

        ; tuvimos que crear las listas de nombres nosotros porque tuvimos problemas con las instancias y el member en listas de instancias (o nombres de instancias)
        (if (and (not (member ?plato1Nombre ?noCompBebida))
            (and (not (member ?plato2Nombre ?noCompBebida))
                 (not (member ?plato3Nombre ?noCompBebida)))) then
            (assert (posibleMenuCorrecto ?platoRec1 ?platoRec2 ?platoRec3 ?bebida))
        )
    )
    (retract ?fact)
)


(defrule genera_solucion::checkMaxMinPriceTrio "Comprobamos la restriccion de precio del menu (ha de estar entre min y max incluidos)"
    (declare (salience 5))
    (informacion_evento (precioMin ?minPrice) (precioMax ?maxPrice))
    ?fact <- (posibleMenuCorrecto ?plato1 ?plato2 ?plato3 ?bebida)
    =>
    (bind ?precioMenu
            (+ (send (send ?plato1 get-plato) get-Plato_precio)
            (+ (send (send ?plato2 get-plato) get-Plato_precio) 
            (+ (send (send ?plato3 get-plato) get-Plato_precio)
               (send ?bebida get-Bebida_precio)))))
    (if (and (<= ?precioMenu ?maxPrice) (>= ?precioMenu ?minPrice)) then
        (make-instance (gensym) of Menu (Menu_primero ?plato1) (Menu_segundo ?plato2) (Menu_postre ?plato3) (Menu_bebida ?bebida) (Menu_precio ?precioMenu))	
    )
    (retract ?fact)
)

(defrule genera_solucion::ordenar_menus_por_precio "Ordenamos las instancias de menu por su precio y los añadimos a una lista"
    (declare (salience 3))
    (not (menusOrdenados ?lista_menus))
    (not (no3Menus))
    =>
    (bind $?menus (find-all-instances ((?inst Menu)) TRUE))
    (printout t "Hemos conseguido generar " (length$ $?menus) " menus correctos" crlf)
    (printout t "Calculando la solucion..." crlf)
    (if (< (length$ ?menus) 3) then
        (assert (no3Menus))
    else
        (bind $?menus_ordenados (sort comparisonPrice $?menus))
        (assert (menusOrdenados $?menus_ordenados))
    )
)

; Partimos la lista de menus en 3 gracias a get_sublista y los indices correspondientes
; Cogemos el menu que tenga más afinidad de cada una de estas listas
; Y lo añadimos a nuestra lista de menus elegidos para ser mostrados
(defrule genera_solucion::get_best_menus "Obtiene 3 menus de distinto precio con la maxima afinidad de cada grupo"
    (declare (salience 2))
    ?menus_ordenados <- (menusOrdenados $?menus)
    (not (no3Menus))
    =>
    (bind ?menus_elegidos (create$))
    (bind ?index1 (round (/ (length$ ?menus) 3)))
    (bind ?index2 (round (* 2 (/ (length$ ?menus) 3))))
    (bind ?sublista1 (get_sublista 1 ?index1 ?menus))
    (bind ?sublista2 (get_sublista ?index1 ?index2 ?menus))
    (bind ?sublista3 (get_sublista ?index2 (length$ ?menus) ?menus))
    (bind ?menu1 (getMenuWithBestAfinity 1 ?sublista1))
    (bind ?menu2 (getMenuWithBestAfinity 2 ?sublista2))
    (bind ?menu3 (getMenuWithBestAfinity 3 ?sublista3))
    (bind $?menus_elegidos (insert$ $?menus_elegidos (+ (length$ $?menus_elegidos) 1) ?menu1))
    (bind $?menus_elegidos (insert$ $?menus_elegidos (+ (length$ $?menus_elegidos) 1) ?menu2))
    (bind $?menus_elegidos (insert$ $?menus_elegidos (+ (length$ $?menus_elegidos) 1) ?menu3))
    (retract ?menus_ordenados)
    (assert (menus_propuestos (menus_elegidos ?menus_elegidos)))
    (printout t "Solucion encontrada con exito" crlf)
)

(defrule genera_solucion::cambiamos_de_modulo "Ya hemos rellenado la lista de los menús y cambiamos de modulo para mostrar la solucion"
    (declare (salience 1))
    (not (no3Menus))
    =>
    (printout t "Modulo de generacion de solucion finalizado" crlf)
    (focus muestra_solucion)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;  MUESTRA SOLUCION   ;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defrule muestra_solucion::imprimir_solucion "Imprime la solucion del problema"
    (menus_propuestos (menus_elegidos $?menus_elegidos))
	=>
    (printout t "Procedemos a mostrar la solucion encontrada" crlf crlf)
    (printout t "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" crlf)
    (printout t "%            Los menus recomendados son los siguientes            %" crlf)
    (printout t "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" crlf)
    (loop-for-count (?i 1 (length$ ?menus_elegidos)) do
	 	(bind ?menu_elegido (nth$ ?i ?menus_elegidos))
        (switch ?i
			(case 1 then
				(printout t "%%%%%% Menu caro recomendado" crlf)
            )
			(case 2 then
				(printout t "%%%%%% Menu de precio medio recomendado" crlf)
            )
            (case 3 then
                (printout t "%%%%%% Menu barato recomendado" crlf)
            )
        )        
        (send ?menu_elegido imprimirMenu)
		(printout t crlf "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" crlf)
    )
    (format t "%nTiempo de usuario: %0.2f segundos %n" (- (time) ?*tiempoInicial*))
    (format t "Tiempo de ejecucion: %0.2f segundos %n%n" (- (time) ?*tiempoEjecucion*))
)
