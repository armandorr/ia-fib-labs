# Practica2 IA - Restaurante RicoRico

## Ejecutar

- Abrir CLIPS en el directorio principal (o utilizar la versión de consola
que ofrece mayor velocidad de ejecución)
- Cargar el programa (load RicoRicoPractica2.clp)
- Recargar las reglas (reset)
- Ejecutar el programa (run)

Responde todas las preguntas para obtener una recomendación para 
nuestro restaurante RicoRico